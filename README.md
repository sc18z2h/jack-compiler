# Jack Compiler

A java program that compiles jack programs.


# Compiler Usage Instructions

1. Change your directory to the root folder<br>
`cd "<Your Directories>/jack-compiler/"`<br>
(where `<Your Directories>` is where you put this folder)


2. Make the bin directory<br>
`mkdir bin`

3. To compile the source code<br>
`javac -d bin src/core/*.java src/utility/*.java`


4. To run the program<br>
`java -cp bin core.JackCompiler <Folder>`<br>
(where `<Folder>` is replaced with the folder to be compiled)
