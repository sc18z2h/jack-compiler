package core;

/** contains header-like empty function definitions of all builtin classes */
public class BuiltinLibrary
{
    /** their function bodies are empty - like a header file */
    public static final String[] builtinClasses = 
        new String[]
        {
            "class Array {"+
            "function Array new(int size) {}"+
            "method void dispose() {} }",

            "class Keyboard {"+
            "function void init() {}"+
            "function char keyPressed() {}"+
            "function char readChar() {}"+
            "function String readLine(String message) {}"+
            "function int readInt(String message) {} }",

            "class Math {"+
            "function void init() {}"+
            "function int abs(int x) {}"+
            "function int multiply(int x, int y) {}"+
            "function int divide(int x, int y) {}"+
            "function int sqrt(int x) {}"+
            "function int max(int a, int b) {}"+
            "function int min(int a, int b) {} }",

            "class Memory {"+
            "function void init() {}"+
            "function int peek(int address) {}"+
            "function void poke(int address, int value) {}"+
            "function int alloc(int size) {}"+
            "function void deAlloc(Array o) {} }",

            "class Output {"+
            "static Array charMaps;"+
            "function void init() {}"+
            "function void initMap() {}"+
            "function void create(int index, int a, int b, int c, int d, int e, int f, int g, int h, int i, int j, int k) {}"+
            "function Array getMap(char c) {}"+
            "function void moveCursor(int i, int j) {}"+
            "function void printChar(char c) {}"+
            "function void printString(String s) {}"+
            "function void printInt(int i) {}"+
            "function void println() {}"+
            "function void backSpace() {} }",

            "class Screen {"+
            "function void init() {}"+
            "function void clearScreen() {}"+
            "function void setColor(boolean b) {}"+
            "function void drawPixel(int x, int y) {}"+
            "function void drawLine(int x1, int y1, int x2, int y2) {}"+
            "function void drawRectangle(int x1, int y1, int x2, int y2) {}"+
            "function void drawCircle(int x, int y, int r) {} }",

            "class String {"+
            "constructor String new(int maxLength) {}"+
            "method void dispose() {}"+
            "method int length() {}"+
            "method char charAt(int j) {}"+
            "method void setCharAt(int j, char c) {}"+
            "method String appendChar(char c) {}"+
            "method void eraseLastChar() {}"+
            "method int intValue() {}"+
            "method void setInt(int val) {}"+
            "function char newLine() {}"+
            "function char backSpace() {}"+
            "function char doubleQuote() {} }",

            "class Sys {"+
            "function void init() {}"+
            "function void halt() {}"+
            "function void wait(int duration) {}"+
            "function void error(int errorCode) {} }"
        };
}