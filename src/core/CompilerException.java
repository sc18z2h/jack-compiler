package core;

import java.io.File;
import java.util.*;

import utility.Token;

public class CompilerException extends Exception
{
    public static List<CompilerException> warnings = new ArrayList<>();

    private static final long serialVersionUID = 1L;

    public CompilerException(String message, Token token)
    {
        super( 
        String.format("%s (%s:%d)",message,
            GetFileName(token.sourceFile), token.lineNumber));
    }

    public CompilerException(String message, File sourceFile, int lineNumber)
    {
        super(
        String.format("%s (%s:%d)",message,
            GetFileName(sourceFile), lineNumber));
    }

    public CompilerException(String firstMessage, Token firstToken,
        String secondMessage, Token secondToken)
    {
        super(
        String.format(
            "%s (%s:%d)\n\t%s (%s:%d)",
            firstMessage, GetFileName(firstToken.sourceFile), firstToken.lineNumber,
            secondMessage,
            firstToken.sourceFile == secondToken.sourceFile ?
                "same file" :
                GetFileName(secondToken.sourceFile),
            secondToken.lineNumber));
    }

    public static void AddWarning(String message, Token token)
    {
        warnings.add(new CompilerException(message, token));
    }

    private static String GetFileName(File file)
    {
        if(file == null)
            return "builtin library";
        else
            return file.getName();
    }
}