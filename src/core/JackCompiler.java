package core;

import java.io.File;

/** entry point for the main compiling program */
public class JackCompiler
{
    public static void main(String[] args)
    {
        //check arguments
        if(args.length < 1)
        {
            System.err.println("invalid usage - requires 1 argument <Folder>");
            return;
        }

        //check the specified directory/folder
        File openDirectory = new File(args[0]);
        if(openDirectory.isDirectory() == false)
        {
            //use a slightly longer path for easier problem solving
            System.err.println(
                String.format("invalid usage - %s is not a folder", openDirectory.getAbsolutePath()));
            return;
        }

        Lexer lexer = new Lexer();
        Parser parser = new Parser();
        VMWriter writer = new VMWriter();
        SemanticAnalyser semanticAnalyser = new SemanticAnalyser(parser, writer);
        writer.ConsoleDebug = false;

        //load built-in libraries
        try
        {
            for (String builtinClass : BuiltinLibrary.builtinClasses)
            {
                lexer.ReadString(builtinClass);
                parser.Parse(lexer.GetAllTokens(), null);
            }
            semanticAnalyser.Evaluate(true);
        }
        catch(Exception e)
        {
            System.err.println("couldn't load builtin library\n"+e.getMessage());
            return;
        }

        //process input
        try
        {
            for (File openClass : openDirectory.listFiles())
            {
                if(openClass.isFile() && openClass.getName().endsWith(".jack"))
                {
                    lexer.ReadFile(openClass);
                    parser.Parse(lexer.GetAllTokens(), openClass);
                }
            }
            semanticAnalyser.Evaluate(false);
        }
        catch(Exception e)
        {
            System.err.println(e.getMessage());
            return;
        }

        //process output
        writer.Compile(openDirectory);

        for (CompilerException warnings : CompilerException.warnings)
            System.err.println(warnings.getMessage());
        
        System.out.println("finished compiling");
    }
}