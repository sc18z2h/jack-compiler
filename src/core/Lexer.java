package core;

import utility.StateMachine;
import utility.Token;

import java.io.*;
import java.util.*;

public class Lexer
{
    private static final String lexerSignature = "lexer error - ";

    private static final String[] keywords = 
    {"class","constructor","method","function","int","boolean","char","void","var",
    "static","field","let","do","if","else","while","return"};
    private static final String[] constants = {"true","false","null"}; //ints are also constants
    private static final String[] identifiers = {"this"};
    private static final char[] symbols =
    {'(', ')', '[', ']','{','}',',',';','=','.','+','-','*','/','&','|','~','<','>'};

    private BufferedReader bufferedReader;

    //these are Finite State Machines that can recognise various things
    private StateMachine commentMachine;
    private StateMachine stringMachine;
    private StateMachine identifierMachine;
    private StateMachine integerMachine;

    /**
     * contains a sequence of characters that are not recognised by the machines yet
     */
    private StringBuilder stringBuilder;
    private int lineCounter;

    /**
     * contains keywords and other constant lexemes that can be recognised without any special logic
     */
    private HashMap<String,Token.TokenTypes> lexicon;
    /**
     * contains special reserved symbols
     */
    private HashSet<Character> symbolLexicon;

    /**
     * contains tokens that should be outputed next, before any more reading is done
     */
    private List<Token> tokenCache;
    /**
     * counter for the peek further function
     */
    private int peekFurtherIndex;

    /**
     * used for storing the char that caused the identifier machine to become invalidated
     */
    private char idCharacterError;

    private File currentFile;

    /**
     * .
     * enables us to avoid unreading the input file
     */
    private Integer setNextCharacter = null;

    /**
     * attempts to build a token from the stringBuilder
     * @return true if token was added, false otherwise
     * @throws CompilerException if lexeme was not recognised
     */
    private boolean TokeniseStringBuilder() throws CompilerException
    {
        if(0 < stringBuilder.length())
        {
            Token.TokenTypes getType;
            String lexeme = stringBuilder.toString();
            if((getType = lexicon.get(lexeme)) != null)
            {
                //its a constant lexeme
                tokenCache.add(new Token(getType,lexeme,lineCounter,currentFile));
            }            
            else
            {
                //could be an identifier
                identifierMachine.Reset();
                integerMachine.Reset();
                for(int i = 0; i < lexeme.length(); i++)
                {
                    identifierMachine.Step(lexeme.charAt(i));
                    integerMachine.Step(lexeme.charAt(i));
                }
                
                if(integerMachine.InFinalState())
                    tokenCache.add(new Token(Token.TokenTypes.constant, lexeme, lineCounter, currentFile));
                else if(identifierMachine.InFinalState())
                    tokenCache.add(new Token(Token.TokenTypes.identifier, lexeme, lineCounter, currentFile));
                else
                {
                    //not identifier, what was the problem?
                    char firstCharacter = lexeme.charAt(0);
                    if(!Character.isLetter(firstCharacter) && firstCharacter != '_')
                        throw new CompilerException(
                            String.format(lexerSignature + "identifier ('%s') can only start with a letter or '_'", lexeme),
                            currentFile,
                            lineCounter);
                    else
                        throw new CompilerException(
                            String.format(lexerSignature + "identifier ('%s') cannot contain '%c' (char int = %d)", 
                                lexeme, idCharacterError, (int)idCharacterError),
                            currentFile,
                            lineCounter);
                }
            }
            //clears the builder
            stringBuilder.setLength(0);

            return true;
        }
        else
            return false;        
    }

    /**
     * attempts to add at least 1 new token to tokenCache
     * @return true if tokenCache was added to, false otherwise
     * @throws IOException if reading file caused an error
     * @throws CompilerException if any token read was invalid
     */
    private boolean PopulateTokenCache() throws IOException, CompilerException
    {
        if(bufferedReader == null)
            return false;

        int readCharacter;        
        Integer commentStart = null;
        boolean inComment = false;
        stringBuilder.setLength(0);

        do
        {
            if(setNextCharacter == null)
            {
                //comment detection
                readCharacter = bufferedReader.read();
                if(readCharacter == '\n')
                    lineCounter++;

                commentMachine.Step((char)readCharacter);
                
                boolean previousInComment = inComment;
                inComment = commentMachine.InFinalState();
                if(commentMachine.GetState() == 1)
                {
                    //could be comment, lets peek ahead
                    setNextCharacter = bufferedReader.read();
                    if(setNextCharacter != -1)
                    {
                        commentMachine.Step((char)setNextCharacter.intValue());
                        if(commentMachine.InFinalState())
                        {
                            //don't include the '/' character we read this iteration
                            setNextCharacter = null;
                            inComment = true;
                        }
                        else
                        {
                            inComment = false;
                        }
                    }
                }

                if(inComment)
                {
                    if(readCharacter == -1)
                    {
                        throw new CompilerException(
                            String.format(lexerSignature + "comment has not terminated"),
                            currentFile, commentStart);
                    }
                    else if(!previousInComment)
                    {
                        //a new comment has started
                        commentStart = lineCounter;
                        TokeniseStringBuilder();
                    }
                    continue;
                }
                //checks for comment ends
                else if(previousInComment)
                {
                    if(0 < tokenCache.size())
                    {
                        //job done
                        return true;
                    }
                    else
                        //nothing was processed, keep going
                        continue;
                }
            }
            else
            {
                readCharacter = setNextCharacter;
                setNextCharacter = null;
            }

            boolean previouslyString = stringMachine.InFinalState();
            stringMachine.Step((char)readCharacter);
            if(!previouslyString && stringMachine.InFinalState())
            {
                //string has started
                TokeniseStringBuilder();
                //don't append the starting character "
                continue;
            }
            else if(previouslyString && !stringMachine.InFinalState())
            {
                //string has terminated
                tokenCache.add(new Token(Token.TokenTypes.string, stringBuilder.toString(), lineCounter, currentFile));
                stringBuilder.setLength(0);
                return true;
            }
            else if(stringMachine.InFinalState())
            {
                //within the string quotes

                //this detects invalid string states
                if((char)readCharacter == '\n')
                {
                    throw new CompilerException(
                        String.format(lexerSignature + "string must not be multi-lined"),
                        currentFile, lineCounter - 1);
                }
                else if(readCharacter == -1)
                    throw new CompilerException(
                        String.format(lexerSignature + "string has not terminated"),
                        currentFile, lineCounter);
            }

            if(stringMachine.InFinalState())
            {
                stringBuilder.append((char)readCharacter);    
                //don't check other logic
                continue;
            }

            if(symbolLexicon.contains((char)readCharacter))
            {
                //encountered a symbol
                TokeniseStringBuilder();
                tokenCache.add(new Token(
                    Token.TokenTypes.symbol, 
                    String.valueOf((char)readCharacter),
                    lineCounter,
                    currentFile));
                return true;
            }
            
            if(Character.isWhitespace((char)readCharacter))
            {
                if(TokeniseStringBuilder())
                    return true;
                else
                    continue;
            }
            else if(readCharacter != -1)
                stringBuilder.append((char)readCharacter);
        }
        while(readCharacter != -1);

        if(readCharacter == -1)
        {
            //file ended, so stop reading file
            bufferedReader.close();
            bufferedReader = null;
        }

        return TokeniseStringBuilder();
    }

    /**
     * creates a buffered reader for generic readers like StringReader or FileReader
     * @param reader the generic reader
     * @param newFile used to produce compilation errors
     */
    private void NewReader(Reader reader, File newFile)
    {    
        bufferedReader = new BufferedReader(reader);
        currentFile = newFile;
        commentMachine.Reset();
        stringMachine.Reset();
        lineCounter = 1;
        tokenCache.clear();
        peekFurtherIndex = 0;
        setNextCharacter = null;
    }

    /**
     * initialises all the data structures
     */
    public Lexer()
    {
        stringBuilder = new StringBuilder();
        tokenCache = new LinkedList<>();

        commentMachine = new StateMachine(
            (currentState, input) -> 
            {
                switch(currentState)
                {
                    case 0: switch(input)
                        {
                            case '/': return 1;                            
                            default: return 0;
                        }
                    case 1: switch(input)
                        {
                            case '/': return 2;
                            case '*': return 3;
                            default: return 0;
                        }
                    case 2: switch(input)
                        {
                            case '\n': return 0;
                            case '\r': return 0;
                            default: return 2;
                        }
                    case 3: switch(input)
                        {
                            case '*': return 4;
                            default: return 3;
                        }
                    case 4: switch(input)
                        {
                            case '/': return 0;
                            default: return 3;
                        }
                    default:
                        return currentState;
                }
            },
            2,3,4);
        
        stringMachine = new StateMachine(
            (currentState, input) -> 
            {
                switch(currentState)
                {
                    case 0: switch(input)
                        {
                            case '"': return 1;                            
                            default: return 0;
                        }
                    case 1: switch(input)
                        {
                            case '"': return 0;
                            default: return 1;
                        }
                    default:
                        return currentState;
                }
            },
            1);
        
        identifierMachine = new StateMachine(
            (currentState, input) ->
            {
                switch(currentState)
                {
                    case 0:
                        if(Character.isLetter(input) || input == '_')
                        {
                            return 1;
                        }
                        else
                            return 2;
                    case 1:
                        if(Character.isLetter(input) || Character.isDigit(input) || input == '_')
                            return 1;
                        else
                        {
                            idCharacterError = input;
                            return 2;
                        }
                    case 2:
                        return 2;
                    default:
                        return currentState;
                }
            },
            1);
        
        integerMachine = new StateMachine((currentState, input) ->
            {
                switch(currentState)
                {
                    case 0:
                        if(Character.isDigit(input))
                            return 1;
                        else
                            return 2;
                    case 1:
                        if(Character.isDigit(input))
                            return 1;
                        else
                            return 2;
                    case 2:
                        return 2;
                    default:
                        return currentState;
                }
            },
            1);

        lexicon = new HashMap<>(keywords.length + constants.length + identifiers.length);        
        for(String keyword : keywords)
            lexicon.put(keyword,Token.TokenTypes.keyword);
        for(String constant : constants)
            lexicon.put(constant,Token.TokenTypes.constant);
        for(String identifier : identifiers)
            lexicon.put(identifier,Token.TokenTypes.identifier);

        symbolLexicon = new HashSet<>(symbols.length);
        for(char symbol : symbols)
            symbolLexicon.add(symbol);        
    }

    /**
     * changes its target to read from a new file
     * @param newFile source of the tokens
     * @throws FileNotFoundException error trying to read newFile
     */
    public void ReadFile(File newFile) throws FileNotFoundException
    {
        if(bufferedReader != null)
            try
            {
                bufferedReader.close();
            }
            catch(IOException exception)
            {
                //its fine, we don't need that reader anymore
                System.err.println("io error closing previous reader\n"+exception.getMessage());
            }
        
        NewReader(new FileReader(newFile), newFile);
    }

    /**
     * changes its target to read from a new string
     * @param string source of the tokens
     */
    public void ReadString(String string)
    {        
        NewReader(new StringReader(string), null);
    }

    /**
     * repeatly call GetNextToken until its tokens are exhausted
     * @return the array of tokens
     * @throws IOException if reading file caused an error
     * @throws CompilerException if any token read was invalid
     */
    public Token[] GetAllTokens() throws IOException, CompilerException
    {
        List<Token> list = new ArrayList<>();
        Token currentToken;

        while((currentToken = GetNextToken()) != null)
            list.add(currentToken);

        Token[] returnArray = new Token[list.size()];
        list.toArray(returnArray);
        return returnArray;
    }

    /**
     * @return the next available token
     * from the input stream,
     * and the token is removed from the input 
     * (i.e. consumed).
     * Or null if reader has reached the end
     * @throws IOException if reading file caused an error
     * @throws CompilerException if any token read was invalid
     */
    public Token GetNextToken() throws IOException, CompilerException
    {
        if(0 < tokenCache.size() || PopulateTokenCache())
        {
            peekFurtherIndex = 0;
            return tokenCache.remove(0);
        }
        else
            return null;
    }

    /**
     * @return the next available token from the input stream, 
     * but the token is not consumed (i.e. it will stay in the input).
     * So, the next time the parser calls GetNextToken, or PeekNextToken, 
     * it gets this same token.
     * Or null if reader has reached the end
     * @throws IOException if reading file caused an error
     * @throws CompilerException if any token read was invalid
     */
    public Token PeekNextToken() throws IOException, CompilerException
    {
        if(0 < tokenCache.size() || PopulateTokenCache())
            return tokenCache.get(0);
        else
            return null;
    }

    /**
     * @return the token following the last token returned by GetNextToken
     * or PeekFurtherToken, whichever one was called last.
     * So, if you call PeekFurtherToken repeatedly, you will recieve consecutive tokens not the same token.
     * Or null if reader has reached the end
     * @throws IOException if reading file caused an error
     * @throws CompilerException if any token read was invalid
     */
    public Token PeekFurtherToken() throws IOException, CompilerException
    {
        if(tokenCache.size() == 0)
        {
            if(PopulateTokenCache())
            {
                peekFurtherIndex = 1;
                return tokenCache.get(0);
            }
            else
                return null;           
        }

        if(peekFurtherIndex < tokenCache.size() || PopulateTokenCache())
        {
            peekFurtherIndex++;
            return tokenCache.get(peekFurtherIndex - 1);
        }
        else
            return null;
    }
}