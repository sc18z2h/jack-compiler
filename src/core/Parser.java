package core;

import utility.GrammarRule;
import utility.ParseTree;
import utility.Token;

//easier to create rules
import static utility.GrammarRule.*;
//easier to create terminals
import utility.Token.TokenTypes;

import java.io.File;

public class Parser
{
    public final String[] primitiveTypes = {"int", "char", "boolean"};

    /**
     * stores the result of the parsing action
     */
    public ParseTree parseTree;

    //these objects are created at compile-time
    public GrammarRule
        classDeclare = new GrammarRule("class declaration"),
        memberDeclare = new GrammarRule("member declaration"),
        type = new GrammarRule(),
        
        classVariableDeclare = new GrammarRule("class variable declaration"),
        subroutineDeclare = new GrammarRule("subroutine declaration"),
        parameterList = new GrammarRule("parameter list"),
        subroutineBody = new GrammarRule("subroutine body"),

        statement = new GrammarRule(),
        variableDeclare = new GrammarRule("variable declaration"),
        letStatement = new GrammarRule("let statement"),
        ifStatement = new GrammarRule("if statement"),
        whileStatement = new GrammarRule("while statement"),
        doStatement = new GrammarRule("do statement"),
        returnStatement = new GrammarRule("return statement"),

        subroutineCall = new GrammarRule("subroutine call"),
        expressionList = new GrammarRule("expression list"),
        
        expression = new GrammarRule("expression"),
        relationalExpression = new GrammarRule("relational"),
        arithmeticExpression = new GrammarRule("arithmetic"),
        term = new GrammarRule("term"),
        factor = new GrammarRule("factor"),
        operand = new GrammarRule("operand");
    
    /**
     * initialises the <code>parseTree</code> and initialises all the grammar rules
     */
    public Parser()
    {
        parseTree = new ParseTree();

        classDeclare.SetRule
        (
            UniqueSequence
            (
                "class",
                TokenTypes.identifier,
                "{",
                ZeroOrMore(memberDeclare),
                "}"
            )
        );

        memberDeclare.SetRule(Choice(classVariableDeclare, subroutineDeclare));

        {
            Object[] possibleTypes = new Object[primitiveTypes.length + 1];
            for(int i = 0; i < primitiveTypes.length; i++)
                possibleTypes[i] = primitiveTypes[i];
            possibleTypes[primitiveTypes.length] = TokenTypes.identifier;
            
            type.SetRule(Choice(possibleTypes));
        }

        classVariableDeclare.SetRule
        (
            UniqueSequence
            (
                Choice("static","field"),
                type,
                TokenTypes.identifier,
                ZeroOrMore(UniqueSequence(",",TokenTypes.identifier)),
                ";"
            )
        );

        subroutineDeclare.SetRule
        (
            Choice
            (
                UniqueSequence
                (
                    Choice
                    (
                        "function",
                        "method"
                    ),
                    Choice
                    (
                        type,
                        "void"
                    ),
                    TokenTypes.identifier,
                    "(",
                    parameterList,
                    ")",
                    subroutineBody
                ),
                UniqueSequence
                (
                    "constructor",
                    type,
                    "new",
                    "(",
                    parameterList,
                    ")",
                    subroutineBody
                )
            )
        );

        parameterList.SetRule
        (
            Choice
            (
                UniqueSequence
                (
                    type,
                    TokenTypes.identifier,
                    ZeroOrMore
                    (
                        UniqueSequence
                        (
                            ",",
                            type,
                            TokenTypes.identifier
                        )
                    )

                ),
                Empty()
            )
        );

        subroutineBody.SetRule
        (
            UniqueSequence
            (
                "{",
                ZeroOrMore(statement),
                "}"
            )
        );

        statement.SetRule
        (
            Choice
            (
                variableDeclare,
                letStatement,
                ifStatement,
                whileStatement,
                doStatement,
                returnStatement
            )
        );

        variableDeclare.SetRule
        (
            UniqueSequence
            (
                "var",
                type,
                TokenTypes.identifier,
                ZeroOrMore
                (
                    UniqueSequence(",",TokenTypes.identifier)
                ),
                ";"
            )
        );

        letStatement.SetRule
        (
            UniqueSequence
            (
                "let",
                TokenTypes.identifier,
                ZeroOrOne
                (
                    UniqueSequence
                    (
                        "[",
                        expression,
                        "]"
                    )
                ),
                "=",
                expression,
                ";"
            )
        );

        ifStatement.SetRule
        (
            UniqueSequence
            (
                "if",
                "(",
                expression,
                ")",
                "{",
                ZeroOrMore(statement),
                "}",
                ZeroOrOne
                (
                    UniqueSequence
                    (
                        "else",
                        "{",
                        ZeroOrMore(statement),
                        "}"
                    )
                )
            )
        );

        whileStatement.SetRule
        (
            UniqueSequence
            (
                "while",
                "(",
                expression,
                ")",
                "{",
                ZeroOrMore(statement),
                "}"
            )
        );

        doStatement.SetRule
        (
            UniqueSequence
            (
                "do",
                subroutineCall,
                ";"
            )
        );

        subroutineCall.SetRule
        (
            UniqueSequence
            (
                TokenTypes.identifier,
                ZeroOrOne
                (
                    UniqueSequence
                    (
                        ".",
                        TokenTypes.identifier
                    )
                ),
                "(",
                expressionList,
                ")"
            )
        );

        expressionList.SetRule
        ( 
            Choice
            (
                Sequence
                (
                    expression,
                    ZeroOrMore
                    (
                        UniqueSequence
                        (
                            ",",
                            expression
                        )                

                    )
                ),
                Empty()
            )
        );

        returnStatement.SetRule
        (
            UniqueSequence
            (
                "return",
                ZeroOrOne(expression),
                ";"
            )
        );

        expression.SetRule
        (
            Sequence
            (
                relationalExpression, 
                ZeroOrMore
                (
                    UniqueSequence
                    (
                        Choice("&","|"),
                        relationalExpression
                    )
                )
            )
        );

        relationalExpression.SetRule
        (
            Sequence
            (
                arithmeticExpression, 
                ZeroOrMore
                (
                    UniqueSequence
                    (
                        Choice("=",">","<"),
                        arithmeticExpression
                    )
                )
            )
        );
        
        arithmeticExpression.SetRule
        (
            Sequence
            (
                term, 
                ZeroOrMore
                (
                    UniqueSequence(Choice("+","-"),term)
                )
            )
        );

        term.SetRule
        (
            Sequence
            (
                factor,
                ZeroOrMore
                (
                    UniqueSequence(Choice("*","/"),factor)
                )
            )
        );

        factor.SetRule
        (
            Choice
            (
                UniqueSequence(Choice("-","~"),operand),
                operand
            )
        );

        operand.SetRule
        (
            Choice
            (
                TokenTypes.constant,//literal int, true, false, null
                TokenTypes.string,
                UniqueSequence
                (
                    TokenTypes.identifier,
                    ZeroOrOne
                    (
                        UniqueSequence
                        (
                            ".",
                            TokenTypes.identifier
                        )
                    ),
                    ZeroOrOne
                    (
                        Choice
                        (
                            UniqueSequence("[",expression,"]"),
                            UniqueSequence("(",expressionList,")")
                        )
                    )
                ),
                UniqueSequence("(",expression,")")
            )
        );
    }

    /**
     * checks the tokens against the grammar rules, and appends to the <code>parseTree</code>
     * @param tokens input tokens
     * @param sourceFile used to produce compilation errors
     * @throws CompilerException if the grammar is invalid
     */
    public void Parse(Token[] tokens, File sourceFile) throws CompilerException
    {
        classDeclare.Evaluate(tokens, parseTree, sourceFile);
    }

    public void ClearParseTree()
    {
        parseTree.Clear();
    }
}