package core;

import java.util.*;

import utility.*;
import utility.ParseTree.Node;

import utility.SymbolTable.*;
import utility.Token.TokenTypes;

public class SemanticAnalyser
{
    /** a generic blueprint to work on a leaf token */
    private interface LeafOperation
    {
        void Operate(Token leaf) throws CompilerException;
    }

    /** for exploring the queue, and know which class we are currently in */
    private class ExploreNode
    {
        public Node node;
        public ClassSymbol itsClass;
        public ExploreNode(Node node, ClassSymbol itsClass)
        {this.node = node; this.itsClass = itsClass;}
    }

    /** stores all the necessary data to evaluate a method */
    private class MethodData
    {
        public ClassSymbol itsClass;
        public MethodSymbol method;
        public Node methodSubtree;
        public MethodData(ClassSymbol itsClass, MethodSymbol method, 
            Node methodSubtree)
        {
            this.itsClass = itsClass;
            this.method = method;
            this.methodSubtree = methodSubtree;
        }
    }

    //used for creating error messages
    private static String semanticError = "semantic error - ";
    private static String semanticWarning = "semantic warning - ";

    private Parser parser;
    private VMWriter writer;
    private SymbolTable symbolTable;
    private ParseTree parseTree;

    /** not an actual type in the symbol table, but its used by type checking functions */
    private TypeSymbol anyType;
    /** not an actual class in the symbol table, but its used by type checking functions */
    private ClassSymbol nullClass;

    /** currently inside a static function (constructor or function) */
    private boolean insideStatic;
    
    /** set of initialised variables so far */
    private Set<String> initialisedVariables;
    private ClassSymbol currentClass;

    /** stores the number of if statements in this function so far, used for code generation */
    private int ifStatementCounter;
    /** stores the number of while statements in this function so far, used for code generation */
    private int whileStatementCounter;

    /**
     * initialise the symbol table and setup the builtin types
     * @param parser the input to get the <code>parseTree</code> from
     * @param writer the output to write to
     */
    public SemanticAnalyser(Parser parser, VMWriter writer)
    {
        this.parser = parser;
        this.parseTree = parser.parseTree;
        this.writer = writer;        

        symbolTable = new SymbolTable();
                
        TypeSymbol[] builtinTypes = 
            new TypeSymbol[parser.primitiveTypes.length];
        for(int i = 0; i < parser.primitiveTypes.length; i++)
        {
            //int, char, bool
            builtinTypes[i] = symbolTable.new TypeSymbol(parser.primitiveTypes[i]);
        }
        
        symbolTable.AddTypes(builtinTypes);

        anyType = symbolTable.new TypeSymbol("anyType");
        nullClass = symbolTable.new ClassSymbol(null, "null");
    }

    /**
     * clear all non-built-in classes
     */
    public void ClearUserClasses()
    {
        symbolTable.ResetTables();
    }

    /**
     * evaluate the semantics currently in the <code>parser</code>.
     * the parse could contain multiple classes from multiple files
     * @param isBuiltinLibrary builtin libraries won't have their function bodies checked and
     * ClearUserClasses won't remove them
     * @throws CompilerException if there's a semantic error
     */
    public void Evaluate(boolean isBuiltinLibrary) throws CompilerException
    {
        //a queue will allow FIFO - breadth first search
        //breadth first search allows all the classes to be declared
        //before evaluating the method bodies
        Queue<ExploreNode> explorationQueue = new LinkedList<>();        
        explorationQueue.add(new ExploreNode(parseTree.root, null));

        ArrayList<MethodData> allMethods = new ArrayList<>();
        
        do
        {
            ExploreNode next = explorationQueue.poll();
            ClassSymbol nextClass = next.itsClass;

            if(IsRule(next.node, parser.classDeclare))
                nextClass = DeclareClass(next.node);
            else if(IsRule(next.node, parser.classVariableDeclare))
            {
                DeclareClassVariable(next.node, nextClass);
                //don't explore its children
                continue;
            }
            else if(IsRule(next.node, parser.subroutineDeclare))
            {
                allMethods.add(
                    new MethodData(
                        nextClass, 
                        DeclareSubroutine(next.node, nextClass),
                        next.node));
                //don't explore its children
                continue;
            }

            //explore its children
            for(Object child : next.node.children)
                if(child instanceof Node)
                    explorationQueue.add(new ExploreNode((Node)child, nextClass));
        } while(explorationQueue.isEmpty() == false);
        
        if(isBuiltinLibrary)
        {
            symbolTable.SetBuiltinClasses();

            //clears everything else (otherwise there will be redefinitions)
            ClearUserClasses();
            parser.ClearParseTree();
            writer.Clear();
            CompilerException.warnings.clear();            
            return;
        }
        
        for (MethodData methodData : allMethods)
        {
            currentClass = methodData.itsClass;
            //load the local variables into the symbol table
            symbolTable.SwitchMethod(methodData.itsClass, methodData.method);
            //change the output file
            writer.ChangeFile(methodData.itsClass.name);

            insideStatic = methodData.method.isStatic &&
                methodData.method.isConstructor == false;
            
            EvaluateMethod(methodData.itsClass,
                methodData.method,
                methodData.methodSubtree);
        }
    }

    /**
     * is <code>object</code> a <code>Node</code> that has the same name as <code>rule</code>
     * @param object checks this is an <code>Node</code>
     * @param rule its rule will be compared against
     * @return true if their names match, false otherwise
     */
    private boolean IsRule(Object object, GrammarRule rule)
    {
        return object instanceof Node && ((Node)object).name != null &&
            ((Node)object).name.equals(rule.toString());
    }

    /**
     * steps through each leaf recursively and carries out an <code>operation</code> until the last index
     * @param startIndex where to begin in the array
     * @param children list of nodes or leaves
     * @param operation the action to perform on each leaf
     * @throws CompilerException if there's a semantic error
     */
    private void ForEachLeaf(int startIndex, 
        ArrayList<Object> children, 
        LeafOperation operation) throws CompilerException
    {
        for(int i = startIndex; i < children.size(); i++)
            if(children.get(i) instanceof Node)
                ForEachLeaf(0, ((Node)children.get(i)).children, operation);
            else
                operation.Operate((Token)children.get(i));
    }

    /**
     * lookup the symbol table for a type with the same name as the token
     * @param nameToken its lexeme will be used
     * @return the type from the table with that name
     * @throws CompilerException if the name dosen't exist
     */
    private TypeSymbol GetTypeSymbol(Token nameToken) throws CompilerException
    {
        TypeSymbol getType = symbolTable.allTypes.get(nameToken.lexeme);
        if(getType == null)
            throw new CompilerException(
                String.format("%s'%s' is unknown", semanticError, nameToken.lexeme),
                nameToken);
        
        return getType;
    }

    /**
     * lookup the symbol table for a variable with the same name as the token.
     * first looks at variables declared in the method, then in the class.
     * @param nameToken its lexeme will be used
     * @param throwStaticError if <code>true</code> throws field symbol static reference errors, 
     * otherwise ignores them (useful for checking the existence of variables)
     * @return the variable from the table with that name
     * @throws CompilerException if there is an invalid static reference error
     */
    private VariableSymbol GetVariable(Token nameToken, boolean throwStaticError)
        throws CompilerException
    {
        //the symbol wouldn't contain "this" if the function is static
        //however the error message would be ambigious ("this" is unknown)
        if(nameToken.lexeme.equals("this") && insideStatic)
        {
            //a better message would be:
            throw new CompilerException(
                String.format("%scannot reference 'this' in static function",
                    semanticError), nameToken);
        }

        VariableSymbol returnSymbol = symbolTable.methodVariables.get(nameToken.lexeme);
        if(returnSymbol == null)
        {
            //variable not declared in method

            FieldSymbol fieldSymbol = symbolTable.classVariables.get(nameToken.lexeme);            

            if(fieldSymbol != null)
            {
                if(throwStaticError &&
                    fieldSymbol.isStatic == false &&
                    insideStatic)
                {
                    throw new CompilerException(
                        String.format("%scannot reference non-static variables in static function",
                            semanticError), nameToken);
                }
            }

            returnSymbol = fieldSymbol;
        }
        return returnSymbol;
    }

    /**
     * add a class to the symbol table using the <code>classDeclare</code> subtree
     * @param subtree this branch is produced by <code>classDeclare</code> from the parser
     * @return the newly created class symbol
     * @throws CompilerException if there's a semantic error
     */
    private ClassSymbol DeclareClass(Node subtree) throws CompilerException
    {
        Token nameToken = (Token)subtree.children.get(1);

        if(nameToken.sourceFile != null)
        {
            //not a builtin class,
            //class name must match file name
            if(nameToken.sourceFile.getName().split("\\.")[0]
                .equals(nameToken.lexeme) == false)
            {
                throw new CompilerException(
                    String.format("%sclass name must match file name",semanticError), 
                    nameToken);
            }
        }

        if(nameToken.lexeme.equals("this"))
        {
            throw new CompilerException(
                String.format("%scannot use identifier 'this' - reserved keyword",
                    semanticError),
                nameToken);
        }

        if(symbolTable.allTypes.containsKey(nameToken.lexeme))
        {
            throw new CompilerException(
                String.format("%s'%s' is already being used",
                    semanticError,
                    nameToken.lexeme),
                nameToken,

                "first declared at",
                symbolTable.allTypes.get(nameToken.lexeme).source);
        }
        else
        {
            ClassSymbol newClass = symbolTable.new ClassSymbol(nameToken, nameToken.lexeme);
            symbolTable.AddTypes(newClass);
            return newClass;
        }
    }

    /**
     * add a class variable to the <code>currentClass</code>
     * @param subtree this branch is produced by <code>classVariableDeclare</code> from the parser
     * @param currentClass the class to add the variable to
     * @throws CompilerException if there's a semantic error
     */
    private void DeclareClassVariable(Node subtree, ClassSymbol currentClass)
        throws CompilerException
    {
        //("static" | "field") type identifier {"," identifier} ";"

        Token getToken;

        //("static" | "field")
        getToken = (Token)subtree.children.get(0);
        boolean isStatic = (getToken.lexeme).equals("static");

        //type
        getToken = (Token)subtree.children.get(1);
        TypeSymbol type = GetTypeSymbol(getToken);

        //identifier {"," identifier}
        //this weird code allows us to step through the parseTree 
        //if the identifers are spread across different subtrees
        //this could happen if the parseTree generates new layers for each rule
        //(although only special rules get new layers, so this code is pretty pointless)
        ForEachLeaf(2, subtree.children,
        (Token leafToken) -> 
        {
            //differentiate identifier vs ","
            if(leafToken.type != TokenTypes.identifier)
                return;
            
            if(leafToken.lexeme.equals("this"))
                throw new CompilerException(
                    String.format("%scannot use identifier 'this' - reserved keyword",
                        semanticError),
                    leafToken);

            //check the name is not taken
            for(FieldSymbol otherField : currentClass.fields)
                if(otherField.name.equals(leafToken.lexeme))
                    throw new CompilerException(
                        String.format("%s'%s' is already being used",
                            semanticError,
                            leafToken.lexeme),
                        leafToken,
                        
                        "first declared at",
                        otherField.source);

            FieldSymbol field = symbolTable.new FieldSymbol(
                leafToken, isStatic, type, leafToken.lexeme);

            //static fields are accessed through the STATIC segment
            //whereas non-static fields are accessed through the THIS segment
            //so their addresses count need to be seperated
            if(isStatic)
            {
                field.address = currentClass.staticVariables;
                currentClass.staticVariables++;
                
                //each static field need a global static getter function
                //because there would be no other way of accessing another class's static fields
                writer.ChangeFile(currentClass.name);
                //the name of the function has a ":"
                //so it will never have the same name as any user defined functions.
                writer.WriteLine(String.format("function %s.:GetStatic%d 0", 
                    currentClass.name, field.address));
                writer.WriteLine("push static "+field.address);
                writer.WriteLine("return");
            }
            else
            {
                field.address = currentClass.nonStaticFields;
                currentClass.nonStaticFields++;
            }

            currentClass.fields.add(field);
        }
        );
    }

    /**
     * add a class subroutine to the <code>currentClass</code>
     * @param subtree this branch is produced by <code>subroutineDeclare</code> from the parser
     * @param currentClass the class to add the subroutine to
     * @return the new method
     * @throws CompilerException if there's a semantic error
     */
    private MethodSymbol DeclareSubroutine(Node subtree, ClassSymbol currentClass)
        throws CompilerException
    {
        //("function" | "method") (type | "void") identifier "(" parameterList ")" "{" subroutineBody "}"

        Token getToken;

        //("function" | "method")
        getToken = (Token)subtree.children.get(0);
        boolean isConstructor = getToken.lexeme.equals("constructor");
        boolean isFunction = getToken.lexeme.equals("function");
        boolean isStatic = isFunction || isConstructor;

        //(type | "void")
        getToken = (Token)subtree.children.get(1);
        TypeSymbol returnType;
        if(isConstructor)
        {
            //constructor can only return its own class's type
            if(getToken.lexeme.equals(currentClass.name) == false)
                throw new CompilerException(
                    String.format("%sconstructor must return '%s'",
                        semanticError, currentClass.name),
                    getToken);
            
            returnType = currentClass;
        }
        else if(getToken.lexeme.equals("void"))
            returnType = null;
        else
            returnType = GetTypeSymbol(getToken);
        
        //identifier
        getToken = (Token)subtree.children.get(2);
        String name = getToken.lexeme;

        if(name.equals("this"))
            throw new CompilerException(
                String.format("%scannot use identifier 'this' - reserved keyword",
                    semanticError),
                getToken);
        
        if(currentClass.name.equals("Main") && name.equals("main"))
        {
            //the entry point must be static
            if(isFunction == false)
                throw new CompilerException(
                    String.format("%sMain.main must be a static function",semanticError),
                    getToken);
        }

        //check the name is not taken
        for(MethodSymbol otherField : currentClass.methods)
            if(otherField.name.equals(name))
                throw new CompilerException(
                    String.format("%s'%s' is already being used",
                    semanticError, name),
                getToken,
                
                "first declared at",
                otherField.source);

        ArrayList<VariableSymbol> parameters = new ArrayList<>();
        if(IsRule(subtree.children.get(4), parser.parameterList))
        {
            //one space is saved for the THIS pointer
            int nextParameterAddress = 1;
            if(isStatic) //static functions dont have THIS pointer
                nextParameterAddress = 0;

            //type identifier {"," type identifier}
            ArrayList<Object> parameterList = ((Node)subtree.children.get(4)).children;
            for(int i = 0; i < parameterList.size(); i += 3)
            {
                TypeSymbol parameterType = GetTypeSymbol((Token)parameterList.get(i));
                Token nameToken = (Token)parameterList.get(i+1);

                //check the name is not taken
                for(VariableSymbol otherParameter : parameters)
                    if(otherParameter.name.equals(nameToken.lexeme))
                        throw new CompilerException(
                            String.format("%s'%s' is already being used",
                                semanticError, nameToken.lexeme),
                            getToken,

                            "first declared at", otherParameter.source);
                
                VariableSymbol newParameter = symbolTable.new VariableSymbol(
                    true, nameToken, parameterType, nameToken.lexeme);
                newParameter.address = nextParameterAddress;
                nextParameterAddress++;
                parameters.add(newParameter);
            }
        }

        MethodSymbol method = symbolTable.new MethodSymbol(
            getToken,
            isStatic,
            isConstructor,
            returnType,
            name,
            parameters);
        currentClass.methods.add(method);
        return method;
    }

    /**
     * evaluate the subroutine body. this can only be done after all the classes have been added to the table
     * @param parentClass the class of the subroutine
     * @param methodSymbol the method in the symbol table
     * @param methodSubtree the subroutine body
     * @throws CompilerException if there's a semantics
     */
    private void EvaluateMethod(ClassSymbol parentClass, MethodSymbol methodSymbol, Node methodSubtree)
        throws CompilerException
    {
        Node methodBody = (Node)methodSubtree.children.get(
            methodSubtree.children.size() - 1);
        
        //keep a set of all variables which have been initialised
        Set<String> initialisedVariables = new HashSet<>(
            methodSymbol.parameters.size()+1);
        
        if(methodSymbol.isStatic == false)
            //this always has a value
            initialisedVariables.add("this");
        
        //every parameter has been initialised
        for(VariableSymbol variable : methodSymbol.parameters)
            initialisedVariables.add(variable.name);
        
        //keep a line for the function declaration in VM code
        //we do this because we don't know how many local variables there are
        //in VM function declaration, you specify the number of local variables
        int functionStartLine = writer.SkipLine();
        if(methodSymbol.isStatic == false)
        {
            //save 2 lines to load THIS segment
            writer.SkipLine();
            writer.SkipLine();
        }
        if(methodSymbol.isConstructor)
        {
            //allocate memory
            writer.WriteLine("push constant "+parentClass.nonStaticFields);
            writer.WriteLine("call Memory.alloc 1");
            writer.WriteLine("pop pointer 0");
        }

        ifStatementCounter = whileStatementCounter = 0;
        boolean returns = EvaluateScope(methodBody.children, 1,
            methodSymbol.returnType, initialisedVariables);

        if(methodSymbol.returnType != null && returns == false)
        {
            //not void AND no returns
            throw new CompilerException(
                String.format("%s'%s' - not all paths return type '%s'",
                    semanticError,
                    ((Token)methodSubtree.children.get(2)).lexeme, 
                    methodSymbol.returnType.name),
                ((Token)methodSubtree.children.get(2)));
        }
        else if(methodSymbol.returnType == null && returns == false)
        {
            //is void AND no returns
            writer.WriteLine("return");
        }

        //in VM function declaration, you specify the number of local variables
        writer.WriteAtLine(
            String.format("function %s.%s %d", parentClass.name, methodSymbol.name,
                symbolTable.GetMaxLocalVariables()),
            functionStartLine);
        if(methodSymbol.isStatic == false)
        {
            //load THIS segment
            writer.WriteAtLine(
                String.format("push argument 0"), functionStartLine+1);
            writer.WriteAtLine(
                String.format("pop pointer 0"), functionStartLine+2);
        }
    }

    /**
     * evaluate a method scope. a new scope is created for every if/while block.
     * @param fringe the method body node
     * @param startIndex where to start looking in the <code>fringe</code>
     * @param returnType the type to expect in a return statement
     * @param initialisedVariables the set of initialised variables when entering this scope
     * @return <code>true</code> if all paths return, <code>false</code> otherwise
     * @throws CompilerException if there's a semantic error
     */
    private boolean EvaluateScope(ArrayList<Object> fringe, int startIndex, 
        TypeSymbol returnType, Set<String> initialisedVariables) throws CompilerException
    {
        this.initialisedVariables = initialisedVariables;

        //starts keeping track of method variables in a new scope
        symbolTable.NewScope();
        boolean returns = false;
        for(int i = startIndex; 
            !(fringe.get(i) instanceof Token && 
            ((Token)fringe.get(i)).lexeme.equals("}"));
            i++)
        {
            Node nextStatement = (Node)fringe.get(i);
            Token getToken;
            VariableSymbol existingVariable;

            if(returns)
            {
                throw new CompilerException(
                    String.format("%sunreachable code", semanticError),
                    ((Token)nextStatement.children.get(0)));
            }

            if(IsRule(nextStatement, parser.variableDeclare))
            {
                //"var" type identifier {"," identifier} ";"

                //type
                getToken = (Token)nextStatement.children.get(1);
                TypeSymbol type = GetTypeSymbol(getToken);

                //identifier {"," identifier}
                for(int j = 2; j < nextStatement.children.size(); j += 2)
                {
                    getToken = (Token)nextStatement.children.get(j);

                    //check for ","
                    if(getToken.type != TokenTypes.identifier)
                        break;
                    
                    String name = getToken.lexeme;

                    if(name.equals("this"))
                    {
                        throw new CompilerException(
                            String.format("%scannot use identifier 'this' - reserved keyword",
                                semanticError),
                            getToken);
                    }

                    //check the name is not taken
                    existingVariable = GetVariable(getToken, false);
                    if(existingVariable == null)
                        symbolTable.AddScopedVariable(
                            symbolTable.new VariableSymbol(false, getToken, type, name));
                    else
                        throw new CompilerException(
                                String.format("%s'%s' is already being used",
                                semanticError, name),
                            getToken,
                            
                            "first declared at",
                            existingVariable.source);
                }
            }
            else if(IsRule(nextStatement, parser.letStatement))
            {
                //"let" identifier ["[" expression "]"] "=" expression ";"

                //identifier
                getToken = (Token)nextStatement.children.get(1);

                //check the name exists
                existingVariable = GetVariable(getToken, true);               
                if(existingVariable == null)
                {
                    throw new CompilerException(
                        String.format("%s'%s' is unknown", semanticError, getToken.lexeme),
                        getToken);
                }
                
                //what we expect the RHS's type to be
                TypeSymbol expectedType = existingVariable.type;

                //are we trying to access an array?
                boolean isArray = false;
                //where is the RHS expression?
                int rhsExpressionIndex = 3;

                getToken = (Token)nextStatement.children.get(2);
                //["[" expression "]"]
                if(getToken.lexeme.equals("["))
                {                    
                    isArray = true;
                    rhsExpressionIndex = 6;

                    //check the LHS is an array object
                    if(existingVariable.type.name.equals("Array") == false)
                    {
                        throw new CompilerException(
                            String.format("%s'%s' is not an Array variable",
                                semanticError, existingVariable.name),
                            ((Token)nextStatement.children.get(1)));
                    }
                    //check the array is initialised
                    //the FieldSymbol type check is because there can be method variables and field variables with the same name,
                    //and when we leave out of a scope, the scope's variables are removed
                    else if(existingVariable instanceof FieldSymbol == false && 
                        initialisedVariables.contains(existingVariable.name) == false)
                    {
                        CompilerException.AddWarning(
                            String.format("%s'%s' not initialised",
                                semanticWarning, existingVariable.name),
                            ((Token)nextStatement.children.get(1)));
                        //suppress any further warnings
                        initialisedVariables.add(existingVariable.name);
                    }                    

                    //anything can be assigned into an element of an array
                    expectedType = anyType;
                }
                
                TypeSymbol rhsType = GetExpressionType(nextStatement.children.get(rhsExpressionIndex));

                //check the types on both sides match
                if(TypeCheck(expectedType, rhsType) == false)
                    CompilerException.AddWarning(
                        String.format("%s'%s' should be assigned '%s' - not '%s'",
                            semanticWarning, existingVariable.name, expectedType.name, rhsType.name),
                        ((Token)nextStatement.children.get(rhsExpressionIndex-1)));

                if(isArray)
                {
                    //compute array index
                    //push index onto the stack
                    TypeSymbol indexType = GetExpressionType(nextStatement.children.get(3));
                    if(TypeCheck(indexType, symbolTable.allTypes.get("int")) == false)
                        CompilerException.AddWarning(
                            String.format("%sArray index should be 'int' - not '%s'",semanticWarning, indexType.name),
                            getToken);                   
                    
                    //push array address onto the stack
                    writer.WriteLine("push "+GetSegmentOffset(existingVariable));
                    
                    //add array index
                    writer.WriteLine("add");
                    writer.WriteLine("pop pointer 1");
                    //assign the RHS to this variable
                    writer.WriteLine("pop that 0");
                }
                else
                {
                    //assign the RHS to this variable
                    writer.WriteLine("pop "+GetSegmentOffset(existingVariable));
                    
                    initialisedVariables.add(existingVariable.name);
                }
            }
            else if(IsRule(nextStatement, parser.ifStatement))
            {
                //"if" "(" expression ")" "{" {statement} "}" ["else" "{" {statement} "}"]

                //expression
                TypeSymbol conditionType = GetExpressionType(nextStatement.children.get(2));
                //check the condition is a boolean type
                if(TypeCheck(conditionType, symbolTable.allTypes.get("boolean")) == false)
                    CompilerException.AddWarning(
                        String.format("%sif condition should be 'boolean' - not '%s'",
                            semanticWarning, conditionType.name),
                        ((Token)nextStatement.children.get(1)));

                //if the condition evaluates to false, jump to the end
                writer.WriteLine("not");
                //line reserved for the goto statement
                int gotoLineNumber = writer.SkipLine();

                //we have write these labels at the end of the block
                String elseLabel = "else"+ifStatementCounter;
                String endIfLabel = "endif"+ifStatementCounter;

                //by default, goto endif
                String gotoLine = "if-goto "+endIfLabel;
                ifStatementCounter++;

                //copy a new set of initialised variables for the true block
                Set<String> trueInitVariables = new HashSet<>(initialisedVariables);

                //does the true block return?
                boolean trueBodyReturns = EvaluateScope(nextStatement.children, 5, 
                    returnType, trueInitVariables);

                //search for the else block
                for(int j = 0; j < nextStatement.children.size(); j++)
                {
                    if(nextStatement.children.get(j) instanceof Token &&
                        ((Token)nextStatement.children.get(j)).lexeme.equals("else"))
                    {
                        //at the end of the true block
                        //unconditional jump to endif
                        writer.WriteLine("goto "+endIfLabel);

                        //change gotoLine to the else block
                        gotoLine = "if-goto "+elseLabel;

                        //mark the start of the else block
                        writer.WriteLine("label "+elseLabel);

                        //copy a new set of initialised variables for the false block
                        Set<String> falseInitVariables = new HashSet<>(initialisedVariables);
                        boolean falseBodyReturns = EvaluateScope(nextStatement.children, j+2, 
                            returnType, falseInitVariables);

                        //if both blocks return these this definitely returns
                        if(trueBodyReturns && falseBodyReturns)
                            returns = true;
                        
                        //initialise variables that are initialised in both blocks
                        //intersection
                        trueInitVariables.retainAll(falseInitVariables);
                        //union
                        initialisedVariables.addAll(trueInitVariables);
                        
                        break;
                    }
                }

                writer.WriteAtLine(gotoLine, gotoLineNumber);
                writer.WriteLine("label "+endIfLabel);
            }
            else if(IsRule(nextStatement, parser.whileStatement))
            {
                //"while" "(" expression ")" "{" {statement} "}"

                String whileLabel = "while"+whileStatementCounter;
                String endWhileLabel = "endwhile"+whileStatementCounter;
                //mark the start of the while block
                writer.WriteLine("label "+whileLabel);
                whileStatementCounter++;

                //expression
                TypeSymbol conditionType = GetExpressionType(nextStatement.children.get(2));
                //the while condition should be type boolean
                if(TypeCheck(conditionType, symbolTable.allTypes.get("boolean")) == false)
                    CompilerException.AddWarning(
                        String.format("%swhile condition should be 'boolean' - not '%s'",
                            semanticWarning, conditionType.name),
                        ((Token)nextStatement.children.get(1)));
                
                //if the condition evaluates to false, jump to the end
                writer.WriteLine("not");
                writer.WriteLine("if-goto "+endWhileLabel);
                
                //evaluate the new scope with a copied set of initialised variables
                EvaluateScope(nextStatement.children, 5, 
                    returnType, new HashSet<>(initialisedVariables));

                writer.WriteLine("goto "+whileLabel);
                writer.WriteLine("label "+endWhileLabel);
            }
            else if(IsRule(nextStatement, parser.doStatement))
            {
                //"do" subroutineCall ";"

                Node subroutineCall = (Node)nextStatement.children.get(1);
                //the subroutineCall is identical to the last part of the operand
                //this name change allows GetExpressionType to work correctly
                subroutineCall.name = parser.operand.toString();
                GetExpressionType(subroutineCall);
                
                //discard the return variable
                writer.WriteLine("pop temp 0");
            }
            else if(IsRule(nextStatement, parser.returnStatement))
            {
                //"return" [expression] ";"

                if(returnType == null)
                {
                    //return;

                    //you don't need to push a value on top of the stack, it dosent do anything

                    if(2 < nextStatement.children.size())
                        throw new CompilerException(
                            String.format("%svoid methods must not return data",semanticError),
                            ((Token)nextStatement.children.get(0)));
                }
                else
                {
                    //return statement;

                    if(nextStatement.children.size() == 2)
                        throw new CompilerException(
                            String.format("%smust return type '%s'",semanticError,returnType.name),
                            ((Token)nextStatement.children.get(0)));
                    
                    TypeSymbol getType = GetExpressionType(nextStatement.children.get(1));
                    //check the return type matches the expression
                    if(TypeCheck(returnType, getType) == false)
                    {
                        CompilerException.AddWarning(
                            String.format("%sshould return type '%s' - not '%s'",
                                semanticWarning, returnType.name, getType.name),
                            ((Token)nextStatement.children.get(0)));
                    }
                }

                writer.WriteLine("return");
                returns = true;
            }
        }

        //discards variables stored in this scope
        symbolTable.ExitScope();
        return returns;
    }

    /**
     * evaluates the type of a expression, and pushes the necessary values onto the stack
     * @param child expression node or a token
     * @return the type of the expression
     * @throws CompilerException if there's a semantic error
     */
    private TypeSymbol GetExpressionType(Object child) throws CompilerException
    {
        Node expression;
        if(child instanceof Token)
        {
            //a single token is treated as a single operand
            expression = parseTree.new Node(null);
            expression.children.add(child);
            expression.name = parser.operand.toString();
        }
        else
            expression = (Node)child;

        if(IsRule(expression, parser.operand))
        {
            //constant | string | 
            //(identifier ["." identifier] [("[" expression "]") | ("(" expressionList ")")] ) |
            //"(" expression ")"

            Token firstToken = (Token)expression.children.get(0);
            if(firstToken.type == TokenTypes.symbol &&
                firstToken.lexeme.equals("("))
            {
                //"(" expression ")"
                return GetExpressionType(expression.children.get(1));
            }
            else if(firstToken.type == TokenTypes.constant ||
                firstToken.type == TokenTypes.string)
            {
                //constant | string
                return GetTokenType(firstToken);
            }
            else if(firstToken.type == TokenTypes.identifier)
            {
                //identifier ["." identifier] [("[" expression "]") | ("(" expressionList ")")]

                //variable/function

                //identifier
                //can be a variable or a class name or a method name
                VariableSymbol variable = GetVariable(firstToken, true);
                TypeSymbol typeSymbol = symbolTable.allTypes.get(firstToken.lexeme);
                MethodSymbol methodSymbol = symbolTable.classMethods.get(firstToken.lexeme);

                if(variable == null && typeSymbol == null && methodSymbol == null)
                    throw new CompilerException(
                        String.format("%s'%s' is unknown", 
                            semanticError, firstToken.lexeme),
                        firstToken);

                if(methodSymbol != null &&
                    methodSymbol.isStatic == false &&
                    insideStatic)
                {
                    throw new CompilerException(
                        String.format("%scannot reference non-static methods in static function",semanticError),
                        firstToken
                    );
                }
                
                //where would the next bracket be?
                int bracketIndex = 1;
                Token token;
                //the class from which the variable from
                //by default its our current class, but it can be an external class
                //eg otherClass.variable
                ClassSymbol sourceClass = currentClass;
                
                if(typeSymbol != null)
                {                    
                    if(expression.children.size() == 1)
                    {
                        //Class
                        throw new CompilerException(
                            String.format("%s'%s' is a class - not a variable", 
                                semanticError, firstToken.lexeme),
                                firstToken);
                    }

                    token = (Token)expression.children.get(1);
                    if(token.lexeme.equals("["))
                    {
                        //Class[]
                        throw new CompilerException(
                            String.format("%s'%s' is not an Array variable",
                                semanticError, typeSymbol.name),
                                token);
                    }
                    else if(token.lexeme.equals("("))
                    {
                        //Class()
                        throw new CompilerException(
                            String.format("%s'%s' is not a method",
                                semanticError, firstToken.lexeme),
                                firstToken);
                    }
                    else
                    {
                        //Class.Function() or Class.static
                        token = (Token)expression.children.get(2);
                        //the bracket is further along
                        bracketIndex = 3;
                        //to use a dot, the type must be a class name
                        if(typeSymbol instanceof ClassSymbol)
                        {
                            //the source has changed to another target
                            sourceClass = (ClassSymbol)typeSymbol;

                            //try to find the name of the static function/method
                            boolean found = false;
                            for (MethodSymbol classMethod : sourceClass.methods)
                            {
                                if(classMethod.name.equals(token.lexeme))
                                {
                                    if(classMethod.isStatic == false)
                                    {
                                        //Class.Method() not static
                                        throw new CompilerException(
                                            String.format("%s'%s' is not static",
                                                semanticError, token.lexeme),
                                            token);
                                    }

                                    methodSymbol = classMethod;
                                    found = true;                                    
                                    break;
                                }
                            }
                            for (FieldSymbol classField : sourceClass.fields)
                            {
                                if(classField.name.equals(token.lexeme))
                                {
                                    if(classField.isStatic == false)
                                    {
                                        //Class.field not static
                                        throw new CompilerException(
                                            String.format("%s'%s' is not static",
                                                semanticError, token.lexeme),
                                            token);
                                    }

                                    variable = classField;
                                    found = true;
                                    break;
                                }
                            }

                            if(found == false)
                            {
                                //Class.Nonexistent()
                                throw new CompilerException(
                                    String.format("%s'%s' is not in class '%s'",
                                        semanticError, token.lexeme, typeSymbol.name),
                                    token);
                            }
                        }
                        else
                        {
                            //primitive.something
                            throw new CompilerException(
                                String.format("%s'%s' is not a class",
                                    semanticError, typeSymbol.name),
                                token);
                        }
                    }
                }

                if(methodSymbol != null)
                {
                    //can be local or static

                    //method(parameters)
                    token = (Token)expression.children.get(bracketIndex-1);

                    if(expression.children.size() <= bracketIndex)
                        //method
                        throw new CompilerException(
                            String.format("%s'%s' is not a variable - its a function",
                                semanticError, token.lexeme),
                            token);
                    
                    if(((Token)expression.children.get(bracketIndex)).lexeme.equals("["))
                    {
                        //method[]
                        throw new CompilerException(
                            String.format("%s'%s' is not an Array",
                                semanticError, token.lexeme),
                            token);
                    }
                    else
                    {
                        //method()
                        int totalParams = methodSymbol.parameters.size();
                        if(methodSymbol.isStatic == false)
                        {
                            //must be a local method
                            writer.WriteLine("push pointer 0");
                            totalParams++;
                        }

                        //checks the parameters passed in
                        //it also pushes the values onto the stack
                        EvaluateMethodCall(methodSymbol,
                            expression.children.get(bracketIndex+1),
                            (Token)expression.children.get(bracketIndex));
                        
                        writer.WriteLine(String.format("call %s.%s %d",
                            sourceClass.name, methodSymbol.name, totalParams));
                        return methodSymbol.returnType;
                    }
                }

                else
                {
                    //variable or variable[expression]
                    token = (Token)expression.children.get(bracketIndex-1);

                    //check the variable is initialised
                    //the FieldSymbol type check is because there can be method variables and field variables with the same name,
                    //and when we leave out of a scope, the scope's variables are removed
                    if(variable instanceof FieldSymbol == false &&
                        initialisedVariables.contains(token.lexeme) == false)
                    {
                        CompilerException.AddWarning(
                            String.format("%s'%s' not initialised",
                                semanticWarning, token.lexeme),
                            token);
                        //suppress any further warnings
                        initialisedVariables.add(token.lexeme);
                    }

                    if(expression.children.size() <= bracketIndex)
                    {
                        writer.WriteLine("push "+GetSegmentOffset(variable));
                        //variable
                        return variable.type;
                    }

                    if(((Token)expression.children.get(bracketIndex)).lexeme.
                        equals("["))
                    {
                        //array[]
                        if(variable.type.name.equals("Array") == false)
                        {
                            //notArray[]
                            throw new CompilerException(
                                String.format("%s'%s' is not Array",
                                    semanticError, token.lexeme),
                                token);
                        }

                        TypeSymbol indexType = GetExpressionType(expression.children.get(bracketIndex+1));
                        if(TypeCheck(indexType, symbolTable.allTypes.get("int")) == false)
                        {
                            //array[notInt]
                            CompilerException.AddWarning(
                                String.format("%sArray index should be 'int' - not '%s'",
                                    semanticWarning, indexType.name),
                                ((Token)expression.children.get(bracketIndex)));
                        }

                        //push array address onto the stack
                        writer.WriteLine("push "+GetSegmentOffset(variable));
                    
                        //add array index
                        writer.WriteLine("add");
                        writer.WriteLine("pop pointer 1");
                        writer.WriteLine("push that 0");

                        //represents any type (primitive/classes)
                        return anyType;
                    }

                    else if(((Token)expression.children.get(bracketIndex)).lexeme.
                        equals("."))
                    {
                        //classVariable.something
                        token = (Token)expression.children.get(bracketIndex+1);
                        typeSymbol = variable.type;
                        //you can only use a dot accessor on a class variable
                        if(typeSymbol instanceof ClassSymbol)
                        {
                            sourceClass = (ClassSymbol)typeSymbol;
                            //find the name of the method/field
                            for (MethodSymbol classMethod : sourceClass.methods)
                            {
                                if(classMethod.name.equals(token.lexeme))
                                {
                                    bracketIndex += 2;
                                    if(expression.children.size() <= bracketIndex ||
                                        expression.children.get(bracketIndex) instanceof Token == false ||
                                        ((Token)expression.children.get(bracketIndex)).lexeme.equals("(") == false)
                                    {
                                        throw new CompilerException(
                                            String.format("%s'%s' is not a variable - its a function",
                                                semanticError, token.lexeme),
                                            token);
                                    }

                                    //we need to know how many parameters there are to call the function
                                    int totalParams = classMethod.parameters.size();
                                    if(classMethod.isStatic == false)
                                    {
                                        //push the address of the calling object onto the stack
                                        writer.WriteLine("push "+GetSegmentOffset(variable));
                                        //increase the number of parameters to accommodate for the extra "this" variable
                                        totalParams++;
                                    }

                                    //push the other arguments onto the stack
                                    EvaluateMethodCall(classMethod,
                                        expression.children.get(bracketIndex+1),
                                        token);
                                    
                                    writer.WriteLine(String.format("call %s.%s %d",
                                        sourceClass.name, classMethod.name, totalParams));
                                    return classMethod.returnType;
                                }
                            }
                            for (FieldSymbol classField : sourceClass.fields)
                            {
                                if(classField.name.equals(token.lexeme))
                                {
                                    if(variable.name.equals("this") == false &&
                                        variable instanceof FieldSymbol &&
                                        ((FieldSymbol)variable).isStatic &&
                                        currentClass != sourceClass)
                                        writer.WriteLine(String.format("call %s.:GetStatic%d 0",
                                            sourceClass.name, variable.address));
                                    else
                                        writer.WriteLine("push "+GetSegmentOffset(variable));
                                    
                                    return classField.type;
                                }
                            }

                            //classVariable.Nonexistent()
                            throw new CompilerException(
                                String.format("%s'%s' is not in class '%s'",
                                    semanticError, token.lexeme, typeSymbol.name),
                                token);
                        }
                        else
                        {
                            //primitive.something
                            throw new CompilerException(
                                String.format("%s'%s' is not a class",
                                    semanticError, token.lexeme),
                                token);
                        }
                    }

                    else
                    {
                        //variable()
                        throw new CompilerException(
                            String.format("%s'%s' is not a method",
                                semanticError, token.lexeme),
                            token);
                    }
                }
            }            
        }

        else if(IsRule(expression, parser.factor))
        {
            // -operand or ~operand
            TypeSymbol type = GetChildType(expression.children.get(1));
            Token token = (Token)expression.children.get(0);
            if(token.lexeme.equals("-"))
            {
                // -operand
                if(TypeCheck(type, symbolTable.allTypes.get("int")) == false)
                    CompilerException.AddWarning(
                        String.format("%sshould only use '-' on type 'int' - not '%s'",
                            semanticWarning, type.name),
                        token);

                writer.WriteLine("neg");
            }
            else
            {
                // ~operand
                if(TypeCheck(type, symbolTable.allTypes.get("boolean")) == false)
                    CompilerException.AddWarning(
                        String.format("%sshould only use '~' on type 'boolean' - not '%s'",
                            semanticWarning, type.name),
                        token);

                writer.WriteLine("not");
            }

            
            return type;
        }

        TypeSymbol expectedType = null;
        //the last symbol token encountered
        Token symbolToken = null;

        if(IsRule(expression, parser.expression))
        {
            //relational & relational | relational
            expectedType = symbolTable.allTypes.get("boolean");
        }
        else if(IsRule(expression, parser.arithmeticExpression) ||
            IsRule(expression, parser.term))
        {
            //number + number - number * number / number
            expectedType = symbolTable.allTypes.get("int");
        }
        else
        {
            //arithmetic = arithmetic > arithmetic < arithmetic
            for(int i = 0; i < expression.children.size(); i++)
            {
                if(i == 0)
                {
                    //every other item should be this type
                    expectedType = GetChildType(expression.children.get(i));
                    continue;
                }

                if(i % 2 == 0)
                {
                    //arithmetic epxression
                    TypeSymbol nextType = GetChildType(expression.children.get(i));
                    if(TypeCheck(expectedType,nextType) == false)
                    {
                        CompilerException.AddWarning(
                            String.format("%sshould not '%s' %s '%s'",
                                semanticWarning, expectedType.name, 
                                symbolToken.lexeme, nextType.name),
                            symbolToken);
                    }

                    if(symbolToken.lexeme.equals("="))
                        writer.WriteLine("eq");
                    else if(symbolToken.lexeme.equals(">"))
                        writer.WriteLine("gt");
                    else if(symbolToken.lexeme.equals("<"))
                        writer.WriteLine("lt");
                }
                else
                {
                    //=, >, <
                    symbolToken = (Token)expression.children.get(i);
                    if(symbolToken.lexeme.equals(">") ||
                        symbolToken.lexeme.equals("<"))
                    {
                        //>, <
                        if(TypeCheck(expectedType, symbolTable.allTypes.get("int")) == false)
                            CompilerException.AddWarning(
                                String.format("%sshould only use '%s' on type 'int' - not '%s'",
                                    semanticWarning, symbolToken.lexeme, expectedType.name),
                                symbolToken);
                    }
                }
            }
            
            return symbolTable.allTypes.get("boolean");
        }

        for(int i = 0; i < expression.children.size(); i++)
        {
            //relational & relational | relational
            //or
            //number + number - number * number / number
            if(i % 2 == 0)
            {
                TypeSymbol nextType = GetChildType(expression.children.get(i));
                if(TypeCheck(expectedType,nextType) == false)
                {
                    if(symbolToken == null)
                        symbolToken = (Token)expression.children.get(i+1);

                    CompilerException.AddWarning(
                        String.format("%sshould only use '%s' on type '%s' - not '%s'",
                            semanticWarning, symbolToken.lexeme, expectedType.name, nextType.name),
                        symbolToken);
                }

                if(0 < i)
                {                    
                    if(symbolToken.lexeme.equals("&"))
                        writer.WriteLine("and");
                    else if(symbolToken.lexeme.equals("|"))
                        writer.WriteLine("or");
                    else if(symbolToken.lexeme.equals("+"))
                        writer.WriteLine("add");
                    else if(symbolToken.lexeme.equals("-"))
                        writer.WriteLine("sub");
                    else if(symbolToken.lexeme.equals("*"))
                        writer.WriteLine("call Math.multiply 2");
                    else if(symbolToken.lexeme.equals("/"))
                        writer.WriteLine("call Math.divide 2");
                }
            }
            else
            {
                symbolToken = (Token)expression.children.get(i);
            }
        }
        
        return expectedType;
    }

    /**
     * returns the type of the token, must be primitive. and pushes its value onto the stack
     * @param token the token in question
     * @return the token's type symbol
     * @throws CompilerException if there's a semantic error
     */
    private TypeSymbol GetTokenType(Token token) throws CompilerException
    {
        if(token.type == TokenTypes.constant)
        {
            //"true" | "false" | "null" | literal int
            if(token.lexeme.equals("true"))
            {
                writer.WriteLine("push constant 1");
                writer.WriteLine("neg");
            }
            else if(token.lexeme.equals("false") ||
                token.lexeme.equals("null"))
                writer.WriteLine("push constant 0");
            else
                //literal int
                writer.WriteLine("push constant "+token.lexeme);

            if(token.lexeme.equals("true") ||
                token.lexeme.equals("false"))
                return symbolTable.allTypes.get("boolean");
            else if(token.lexeme.equals("null"))
                return nullClass;
            else
                return symbolTable.allTypes.get("int");
        }
        else if(token.type == TokenTypes.string)
        {
            //literal string
            //create string object
            writer.WriteLine("push constant "+token.lexeme.length());
            writer.WriteLine("call String.new 1");

            for(int i = 0; i < token.lexeme.length(); i++)
            {
                writer.WriteLine("push constant "+(int)token.lexeme.charAt(i));
                //String.appendChar returns the same object that we passed in,
                //so we don't have to push the string onto the stack again
                writer.WriteLine("call String.appendChar 2");
            }

            return symbolTable.allTypes.get("String");
        }
        else
            return GetExpressionType(token);
    }

    /**
     * get the type of a sub-section of the parsetree
     * @param child a Node or Token object
     * @return the section's type
     * @throws CompilerException if there's a semantic error
     * @throws RuntimeException if child is not a Node or a Token
     */
    private TypeSymbol GetChildType(Object child) throws CompilerException
    {
        if(child instanceof Node)
            return GetExpressionType(child);
        else if(child instanceof Token)
            return GetTokenType((Token)child);
        else
            throw new RuntimeException("not a parse tree child");
    }

    /**
     * are these types compatible
     * eg for operations or assignment
     * @param a
     * @param b
     * @return true if they are compatible, false otherwise
     */
    private boolean TypeCheck(TypeSymbol a, TypeSymbol b)
    {
        if(a == anyType || b == anyType)
            return true;
        else if(a == nullClass || b == nullClass)
        {
            //null is compatible with any Class, 
            //but null cannot be equated to primitive types
            return a instanceof ClassSymbol == true &&
                b instanceof ClassSymbol == true;
        }
        else
        {
            return a == b ||
                //int and char are interchangable
               ((a.name.equals("int") || a.name.equals("char")) &&
                (b.name.equals("int") || b.name.equals("char")));
        }
    }

    /**
     * evaluate the parameter list of a method call and pushes their value onto the stack
     * @param methodSymbol the method that this is called from
     * @param expressionList can be a Token or a Node
     * @param lastToken the last token encountered before the parameter list.
     * used for creating error messages.
     * @throws CompilerException if there's a semantic error
     */
    private void EvaluateMethodCall(MethodSymbol methodSymbol, Object expressionList, Token lastToken)
        throws CompilerException
    {
        //"(" expression {"," expression} ")"

        //something that we can step through with a for loop
        List<Object> normalisedList;
        
        if(expressionList instanceof Token)
        {            
            //convert the Token into a list form
            normalisedList = new ArrayList<>(1);
            Token castToken = (Token)expressionList;

            //a type of symbol means it's a ")".
            //so the parameter list is empty
            if(castToken.type != TokenTypes.symbol)
            {                
                normalisedList.add(castToken);
                lastToken = castToken;
            }
        }
        else
        {
            //if there's only 1 parameter, the expression list node is trimmed off
            //so its children is brought up 1 level
            Node castNode = (Node)expressionList;
            if(IsRule(castNode, parser.expressionList))
                normalisedList = ((Node)expressionList).children;
            else
            {
                //there will be only 1 parameter
                //expressionList will be of type expression
                normalisedList = new ArrayList<>(1);
                normalisedList.add(expressionList);
            }
        }

        //a counter to keep track of how many parameters there are
        int parameterIndex = 0;

        for(int i = 0; i < normalisedList.size(); i++)
        {
            if(i%2 == 1)
            {
                //at odd positions, there will be ","
                lastToken = (Token)normalisedList.get(i);
                continue;
            }

            if(methodSymbol.parameters.size() <= parameterIndex)
                throw new CompilerException(
                    String.format("%stoo many parameters", semanticError),
                lastToken);

            TypeSymbol expressionType = GetExpressionType(normalisedList.get(i));
            //check the expression matches the expected type of the parameter
            if(TypeCheck(expressionType,
                methodSymbol.parameters.get(parameterIndex).type) == false)
                CompilerException.AddWarning(
                    String.format("%sparameter #%d should be '%s' - not '%s'",
                        semanticWarning, parameterIndex+1,
                        methodSymbol.parameters.get(parameterIndex).type.name,
                        expressionType.name),
                    lastToken);
            
            parameterIndex++;
        }

        //finally, check that there were enough parameters
        if(parameterIndex < methodSymbol.parameters.size())
            throw new CompilerException(
                String.format("%stoo few parameters - expected type '%s' next",
                    semanticError, methodSymbol.parameters.get(parameterIndex).type.name),
                lastToken);
    }

    /**
     * get the segment name and its address offset of a variable
     * @param variable can be "this" or a FieldSymbol or a parameter or
     * a local scope variable
     * @return
     */
    private String GetSegmentOffset(VariableSymbol variable)
    {
        if(variable.name.equals("this"))
            //inside the pointer segment
            return "pointer 0";
        else if(variable instanceof FieldSymbol)
        {
            if(((FieldSymbol)variable).isStatic)
                //inside static segment
                return "static "+variable.address;
            else
                //inside THIS segment
                return "this "+variable.address;
        }
        else if(variable.isParameter)
            //inside parameter segment
            return "argument "+variable.address;
        else
            //inside local segment
            return "local "+variable.address;
    }
}