package core;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class VMWriter
{
    /** if true, outputs to the console instead of a file */
    public boolean ConsoleDebug = false;

    /** dictionary of files to write, the file's name and its lines of code
     * the list of string value represents lines of code to write
     */
    private Map<String,List<String>> vmFiles;
    /** the current file's code lines */
    private List<String> currentFile;

    public VMWriter()
    {
        vmFiles = new HashMap<>();
    }

    /** used for testing and other miscellaneous purposes */
    public void Clear()
    {
        vmFiles.clear();
    }

    /**
     * creates a new entry on the dictionary if the fileName dosen't exist,
     * otherwise just switches over
     * @param fileName the name of the class and file
    */
    public void ChangeFile(String fileName)
    {
        currentFile = vmFiles.get(fileName);
        //null entries on the dictionary represent no file exists yet
        if(currentFile == null)
        {
            currentFile = new ArrayList<>();
            vmFiles.put(fileName, currentFile);
        }
    }

    /**
     * write a line of code in the current file
     * @param line of code
     */
    public void WriteLine(String line)
    {
        currentFile.add(line);
    }

    /**
     * reserve a line of code to be written later.
     * useful if you need more information to write the code.
     * @return the line number reserved
     */
    public int SkipLine()
    {
        int lineNumber = currentFile.size();
        currentFile.add(null);
        return lineNumber;
    }

    /**
     * write a line of code at a previously line.
     * @param line the line of code
     * @param lineNumber the line at which to write it
     */
    public void WriteAtLine(String line, int lineNumber)
    {
        currentFile.set(lineNumber, line);
    }

    /**
     * write all the defined vm files to the designated output
     * @param outputDirectory which folder to write out to? this variable is functionless if ConsoleDebug=true.
     */
    public void Compile(File outputDirectory)
    {
        for(Map.Entry<String, List<String>> entry : vmFiles.entrySet())
        {
            String vmFileName = entry.getKey()+".vm";
            String fileContent = CompileFile(entry.getValue());
            if(ConsoleDebug)
            {
                System.out.println(vmFileName);
                System.out.println(fileContent);
            }
            else if(outputDirectory.isDirectory())
            {
                try
                {
                    FileWriter myWriter = new FileWriter(outputDirectory.toPath() + "/" + vmFileName);
                    myWriter.write(fileContent);
                    myWriter.close();
                }
                catch (IOException e)
                {
                    //print the output, but keep going
                    System.out.println(String.format("%s error - %s",vmFileName,e.getMessage()));
                }
            }
        }
    }

    /**
     * convert the list of code lines representing a file to a singular string
     * @param file the file to convert
     * @return a single string with new lines representing the whole file
     */
    private String CompileFile(List<String> file)
    {
        //use a string builder for memory efficiency
        StringBuilder builder = new StringBuilder();

        for(int i = 0; i < file.size(); i++)
        {
            //optimisation - reduce vm code without removing functionality
            if((i+1) < file.size())
            {
                //there's at least 1 more line left
                //these optimisations require this

                if(file.get(i).equals("not") && 
                    file.get(i+1).equals("not"))
                {
                    //double negation
                    //increment i to skip the next "not" statement
                    i++;
                    continue;
                }

                else if(file.get(i).startsWith("goto") &&
                    file.get(i+1).startsWith("label") && 
                    file.get(i).substring(5).equals(file.get(i+1).substring(6)))
                {
                    //goto the next line can be removed
                    //don't increment i, because some other jump may require this label
                    continue;
                }
            }

            String nextLine = file.get(i);

            //some formating to make the output code easier to read - for debugging
            if(nextLine.startsWith("function") == false &&
                nextLine.startsWith("label") == false)
                builder.append("\t");
            
            builder.append(nextLine);
            //new line is appended at the end of each code line
            builder.append("\n");
        }

        return builder.toString();
    }
}