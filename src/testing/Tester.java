package testing;

import core.*;
import utility.*;

import java.io.*;
import java.util.*;

public class Tester
{
    private static int failedTests;

    //returns true if test succeeds
    private static boolean CompareTest(String moduleName, String fault, Object expected, Object actual)
    {
        if(expected.equals(actual) == false)
        {
            failedTests++;
            System.out.println(
                String.format("%s failed\n(%s)\n\texpected >> %s\n\tactual   >> %s",
                moduleName, fault, expected.toString(), actual.toString()));
            return false;
        }
        else
            return true;
    }

    private static void FailTest(String moduleName, String fault)
    {
        failedTests++;
        System.out.println(
            String.format("%s failed\n(%s)",
            moduleName, fault));
    }

    private static List<String> ReadCsv(File file)
    {
        List<String> result = new ArrayList<String>();
        BufferedReader bufferedReader;
        try
        {
            bufferedReader = new BufferedReader(
                new FileReader(file));    
        }
        catch (FileNotFoundException exception)
        {
            System.out.println("could not read csv file\n"+exception.getMessage());
            return result;
        }
        
        String readLine;
        StringBuilder builder = new StringBuilder();
        boolean quoted = false;
        try
        {
            while((readLine = bufferedReader.readLine()) != null)
            {
                for(int i = 0; i < readLine.length(); i++)
                {
                    char character = readLine.charAt(i);
                    if(character == '"')
                    {
                        quoted = !quoted;
                        continue;
                    }
                    if(quoted == false)
                    {
                        if(Character.isWhitespace(character))
                            continue;
                        else if(character == ',')
                        {
                            if(0 < builder.length())
                            {
                                result.add(builder.toString());
                                builder.setLength(0);
                            }
                            continue;
                        }
                    }

                    builder.append(character);
                }

                if(quoted == false && 0 < builder.length())
                {
                    result.add(builder.toString());
                    builder.setLength(0);
                }
            }

            bufferedReader.close();
            return result;
        }
        catch(IOException exception)
        {
            System.out.println("error reading csv file\n"+exception.getMessage());
            return result;
        }
    }

    public static void main(String[] args)
    {
        failedTests = 0;

        System.out.println("running tests ...");
        System.out.println();

        LexerTest();
        CodeGenerationTest();
        System.out.println();

        System.out.println("failed tests = "+failedTests);
    }

    private static boolean AnalyseToken(Token token, String typeName, String lexeme, String testFile, String method)
    {
        if(CompareTest("lexer "+testFile, method+" returned incorrect token type", typeName, token.type.toString())
            == false)
        {
            return false;
        }
        else if(CompareTest("lexer " + testFile, method+" returned incorrect lexeme", lexeme, token.lexeme)
            == false)
        {
            return false;
        }
        else return true;
    }

    public static List<File[]> ReadTestFolder(String folderDirectory)
    {
        List<File[]> returnFiles = new ArrayList<File[]>();

        File testDataFolder = new File(folderDirectory);
        for(File file : testDataFolder.listFiles())
        {
            String[] nameParts = file.getName().split("-");
            if(nameParts[0].equals("test"))
            {
                File[] pair = new File[2];
                pair[0] = file;

                String[] nameEnd = nameParts[1].split("\\.");
                pair[1] = new File(String.format("%s/expected-%s.csv", testDataFolder.getPath(), nameEnd[0]));

                returnFiles.add(pair);
            }
        }

        return returnFiles;
    }

    public static void LexerTest()
    {
        List<File[]> testFiles = ReadTestFolder("./test data/lexer tests");

        for(File[] pair : testFiles)
        {
            File testFile = pair[0];
            File expectedFile = pair[1];

            Lexer lexer;
            try
            {
                lexer = new Lexer();
                lexer.ReadFile(testFile);
            }
            catch(FileNotFoundException exception)
            {
                System.out.println("error trying to open test file\n"+exception.getMessage());
                continue;
            }

            List<String> csv = ReadCsv(expectedFile);

            int i = 0;
            Token token;
            try
            {
                if((token = lexer.PeekNextToken()) != null)
                {
                    Token nextToken = lexer.PeekNextToken();

                    if(AnalyseToken(token, csv.get(i), csv.get(i+1), testFile.getName(), "PeekNextToken") == false)
                    {   continue; /*error message has already been issued inside AnalyseToken */ }
                    else if(CompareTest("lexer " + testFile.getName(), 
                        "PeekNextToken has consumed token", token, nextToken) == false)
                    {                            
                        continue;
                    }
                }

                Token furtherToken = lexer.PeekFurtherToken();
                if(CompareTest("lexer " + testFile.getName(),
                    "PeekFurtherToken has not matched PeekNextToken", token, furtherToken) == false)
                {
                    continue;
                }
                else if(token != null)
                {
                    //this token should be advanced
                    Token nextToken = lexer.PeekFurtherToken();
                    if(AnalyseToken(nextToken, csv.get(i+2), csv.get(i+3), testFile.getName(), "PeekFurtherToken") == false)
                        continue;
                }
                
                while((token = lexer.GetNextToken()) != null)
                {
                    if(AnalyseToken(token, csv.get(i), csv.get(i+1), testFile.getName(), "GetNextToken") == false)
                        continue;
                    i += 2;
                }
            }
            catch(CompilerException exception)
            {
                CompareTest("lexer "+testFile.getName(), "unexpected compiler error", csv.get(i), exception.getMessage());
            }
            catch(IOException exception)
            {
                FailTest("lexer " + testFile.getName(), "io error when reading test file\n"+exception.getMessage());
            }
        }
    }

    public static void CodeGenerationTest()
    {
        CompileFolders(new File("./test data/Valid Jack Programs"), true,
            new File("./test data/Valid Jack Programs/Set 4"),
            new File("./test data/semantic tests"),
            "code generation");
    }

    private static void CompileFolders(File validFolder, boolean subFolders, 
        File libraryFolder, File invalidFolder, String moduleName)
    {
        Lexer lexer = new Lexer();
        Parser parser = new Parser();
        VMWriter writer = new VMWriter();
        SemanticAnalyser semanticAnalyser = new SemanticAnalyser(parser, writer);
        
        for(File sourceFile : libraryFolder.listFiles())
        {
            if(sourceFile.isDirectory()) continue;
            if(sourceFile.getName().split("\\.")[1].equals("jack") == false)
                continue;            
            Stage1(sourceFile, lexer, parser);
        }
         
        Stage2(semanticAnalyser, true);

        List<File> compileFolders = new ArrayList<>();

        if(subFolders)
        for(File setFolder : validFolder.listFiles())
        {
            if(setFolder.isFile()) continue;
            for(File programFolder : setFolder.listFiles())
            {
                if(programFolder.isFile()) continue;                
                compileFolders.add(programFolder);
            }
        }
        else
            compileFolders.add(validFolder);

        for(File programFolder : compileFolders)
        {
            parser.ClearParseTree();
            semanticAnalyser.ClearUserClasses();
            CompilerException.warnings.clear();
            writer.Clear();

            boolean succeed = true;
            for(File sourceFile : programFolder.listFiles())
            {
                if(sourceFile.getName().split("\\.")[1].equals("jack") == false)
                    continue;
                
                Exception e = Stage1(sourceFile, lexer, parser);
                if(e != null)
                {
                    FailTest(moduleName, sourceFile.getPath() + "\n" + e.getMessage());
                    succeed = false;
                    return;
                }
            }

            if(succeed)
            {
                Exception e = Stage2(semanticAnalyser, false);
                if(e == null)
                {
                    for(CompilerException warning : CompilerException.warnings)
                    {
                        //System.out.println(warning.getMessage());
                        CompareTest("semantic", "warning isn't called warning", true, 
                            warning.getMessage().contains("warning"));
                    }                    
                }
                else
                {
                    FailTest("semantic", "problem in folder "+programFolder.getPath()+"\n"+e.getMessage());
                    return;
                }
            }

            writer.Compile(programFolder);
            System.out.println("compiled folder : "+programFolder.getPath());
        }

        int count = 0;

        if(invalidFolder == null)
            return;
            
        for(File jackSource : invalidFolder.listFiles())
        {
            count++;

            semanticAnalyser.ClearUserClasses();
            if(count != 2)
            {
                parser.ClearParseTree();
                CompilerException.warnings.clear();
            }

            Exception e = null;

            if(CompareTest(moduleName, "stage 1 failed", true,
                    (e = Stage1(jackSource, lexer, parser)) == null))
            {
                e = Stage2(semanticAnalyser, false);

                if(e != null)
                {
                    //System.out.println(e.getMessage());
                }
                
                else if(CompilerException.warnings.size() == 0)
                    FailTest(moduleName, "missed errors in "+jackSource.getPath());

                for(CompilerException warning : CompilerException.warnings)
                {
                    //System.out.println(warning.getMessage());
                    CompareTest("semantic", "warning isn't called warning : "+warning.getMessage(), true, 
                        warning.getMessage().contains("warning"));
                }
                continue;
            }
            else
            {
                if(e != null)
                    System.out.println(e.getMessage());
                return;
            }
        }
    }
    
    private static Exception Stage1(File sourceFile, 
        Lexer lexer, Parser parser)
    {
        try
        {
            lexer.ReadFile(sourceFile);
        }
        catch(FileNotFoundException e)
        {
            return e;
        }

        List<Token> allTokens = new ArrayList<Token>();           
        
        try
        {            
            try
            {
                Token readToken;
                while((readToken = lexer.GetNextToken()) != null)
                {
                    allTokens.add(readToken);
                }
            }
            catch(IOException e)
            {
                return e;
            }

            Token[] tokenArray = new Token[allTokens.size()];
            allTokens.toArray(tokenArray);
            parser.Parse(tokenArray, sourceFile);
        }
        catch(CompilerException error)
        {            
            return error;
        }

        return null;
    }

    private static Exception Stage2(SemanticAnalyser semanticAnalyser, boolean isBuiltinLibrary)
    {
        try
        {
            semanticAnalyser.Evaluate(isBuiltinLibrary);
            return null;
        }
        catch(CompilerException error)
        {
            return error;
        }
    }
}