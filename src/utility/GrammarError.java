package utility;
import java.util.*;

/** allows for tracing a error in grammar rules */
public class GrammarError
{
    /** every error message issued by the whole chain of rules */
    public List<String> errorMessages;
    /** where the error occured */
    public int lineNumber;
    /** if true, all rules will stop processing immediately */
    public boolean isCritical;

    public GrammarError(String singleError, int line)
    {
        errorMessages = new ArrayList<String>();
        errorMessages.add(singleError);
        lineNumber = line;
    }
}