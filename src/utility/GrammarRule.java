package utility;

import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;
import core.CompilerException;

public class GrammarRule
{
    /**
     * the number of special rules to trace back upon an error
     */
    public static final int ERROR_DEPTH = 2;

    private static Token[] allTokens;
    private static ParseTree parseTree;

    /**
     * representation of eBNF's epsilon
     * @return a rule that accepts empty string (ie anything) but also consumes nothing
     */
    public static GrammarRule Empty()
    {
        GrammarRule grammarRule = new GrammarRule();
        grammarRule.rule = grammarRule::ParseEmpty;
        grammarRule.arguments = null;
        grammarRule.name = "nothing";
        return grammarRule;
    }

    /**
     * representation of eBNF's concatenation
     * @param arguments can be any of: <code>String</code> (matches lexeme), 
     * <code>TokenType</code>, <code>GrammarRule</code>
     * @return a rule that only accepts these arguments in their order
     */
    public static GrammarRule Sequence(Object ... arguments)
    {
        CheckArguments(arguments);
        GrammarRule grammarRule = new GrammarRule();
        grammarRule.rule = grammarRule::ParseSequence;
        grammarRule.arguments = arguments;

        StringBuilder builder = new StringBuilder();
        BuildBracketList(builder, arguments, '(', ')', ", ");
        
        grammarRule.name = builder.toString();
        return grammarRule;
    }

    /**
     * representation of eBNF's concatenation.
     * difference to ordinary <code>Sequence</code> is that once the first argument is matched,
     * any mismatched argument is assumed to be a critical error
     * @param arguments can be any of: <code>String</code> (matches lexeme),
     * <code>TokenType</code>, <code>GrammarRule</code>
     * @return a rule that only accepts these arguments in their order
     */
    public static GrammarRule UniqueSequence(Object ... arguments)
    {
        CheckArguments(arguments);
        GrammarRule grammarRule = new GrammarRule();
        grammarRule.rule = grammarRule::ParseUniqueSequence;
        grammarRule.arguments = arguments;

        StringBuilder builder = new StringBuilder();
        BuildBracketList(builder, arguments, '(', ')', ", ");
        
        grammarRule.name = builder.toString();
        return grammarRule;
    }

    /**
     * representation of eBNF's "|" (alternation)
     * @param arguments can be any of: <code>String</code> (matches lexeme), 
     * <code>TokenType</code>, <code>GrammarRule</code>
     * @return a rule that accepts any one of its arguments
     */
    public static GrammarRule Choice(Object ... arguments)
    {
        CheckArguments(arguments);
        GrammarRule grammarRule = new GrammarRule();
        grammarRule.rule = grammarRule::ParseChoice;
        grammarRule.arguments = arguments;

        StringBuilder builder = new StringBuilder();
        BuildBracketList(builder, arguments, '(', ')', " | ");
        
        grammarRule.name = builder.toString();
        return grammarRule;
    }

    /**
     * representation of eBNF's "[]" (optional)
     * @param argument can be any of: <code>String</code> (matches lexeme), 
     * <code>TokenType</code>, <code>GrammarRule</code>
     * @return a rule that accepts zero or one occurence of the argument
     */
    public static GrammarRule ZeroOrOne(Object argument)
    {
        CheckArguments(argument);
        GrammarRule grammarRule = new GrammarRule();
        grammarRule.rule = grammarRule::ParseZeroOrOne;
        grammarRule.arguments = new Object[] {argument};
        grammarRule.name = String.format("[ %s ]",argument);
        return grammarRule;
    }

    /**
     * this dosen't exist in eBNF, custom representation "<>"
     * @param argument can be any of: <code>String</code> (matches lexeme), 
     * <code>TokenType</code>, <code>GrammarRule</code>
     * @return a rule that accepts one or more occurences of the argument
     */
    public static GrammarRule OneOrMore(Object argument)
    {
        CheckArguments(argument);
        GrammarRule grammarRule = new GrammarRule();
        grammarRule.rule = grammarRule::ParseOneOrMore;
        grammarRule.arguments = new Object[] {argument};
        grammarRule.name = String.format("< %s >",argument);
        return grammarRule;
    }

    /**
     * representation of eBNF's "{}" (repetition)
     * @param argument can be any of: <code>String</code> (matches lexeme), 
     * <code>TokenType</code>, <code>GrammarRule</code>
     * @return a rule that accepts zero or more occurences of the argument
     */
    public static GrammarRule ZeroOrMore(Object argument)
    {
        CheckArguments(argument);
        GrammarRule grammarRule = new GrammarRule();
        grammarRule.rule = grammarRule::ParseZeroOrMore;
        grammarRule.arguments = new Object[] {argument};
        grammarRule.name = String.format("{ %s }",argument);
        return grammarRule;
    }

    /**
     * a range checking getter for <code>allTokens</code>
     * @param atIndex
     * @return the token at the index or <code>null</code> if its out of range
     */
    private static Token GetToken(int atIndex)
    {
        if(allTokens.length <= atIndex)
            return null;
        else
            return allTokens[atIndex];
    }

    /**
     * used for finding the end of file location 
     * @return the last line number in <code>allTokens</code> or 0 if it's empty
     */
    private static int GetLastLine()
    {
        return 0 < allTokens.length ?
            allTokens[allTokens.length - 1].lineNumber :
            0;
    }

    /**
     * used for finding the line number of a token that we're unsure if it exists
     * @param tokenIndex
     * @return the line number of the token at <code>tokenIndex</code> or the last line if its out of range
     */
    private static int GetLine(int tokenIndex)
    {
        return tokenIndex < allTokens.length ?
            allTokens[tokenIndex].lineNumber :
            GetLastLine();
    }

    /**
     * confirms the arguments are valid for creating a rule with
     * @param arguments members are either <code>String</code> or <code>TokenTypes</code> or <code>GrammarRule</code>
     */
    private static void CheckArguments(Object ... arguments)
    {
        if(arguments == null)
            throw new RuntimeException("grammar rule : arguments cannot be null");
        else for(Object argument : arguments)
            if(argument == null)
                throw new RuntimeException("grammar rule : arguments cannot be null");
            else if(argument instanceof String || argument instanceof Token.TokenTypes || argument instanceof GrammarRule)
                continue;
            else
                throw new RuntimeException(
                "grammar rule has invalid argument type (" + argument.getClass().getName() + ")\n" +
                "grammar rule's possible argument types : "+
                "String (lexeme), TokenType, GrammarRule");
    }

    /**
     * scans through <code>allTokens</code> beginning at <code>startIndex</code>, 
     * then checks it matches the <code>argument</code>, and outputs to <code>endIndex</code>
     * @param argument can be any of: <code>String</code> (matches lexeme), 
     * <code>TokenType</code>, <code>GrammarRule</code>
     * Matches to a token
     * @param startIndex where to start scanning tokens
     * @param endIndex outputs the endIndex
     * @return <code>null</code> if <code>argument</code> matches, error message otherwise
     */
    private static GrammarError EvaluateArgument(Object argument, int startIndex, AtomicInteger endIndex)
    {
        CheckArguments(argument);

        boolean isString = argument instanceof String,
                isTokenType = argument instanceof Token.TokenTypes;

        if(isString || isTokenType)
        {
            Token nextToken = GetToken(startIndex);
            if(nextToken == null)
            {
                //there are no more tokens
                String quotedArgument = isString ?
                String.format("'%s'",argument) : argument.toString();

                return new GrammarError(
                    String.format("expected %s, but file ended",
                        quotedArgument),
                    GetLastLine());
            }

            //the object to compare the argument to
            Object compareTo = null;
            if(isString)
                compareTo = nextToken.lexeme;
            else if(isTokenType)
                compareTo = nextToken.type;
            
            if(argument.equals(compareTo))
            {
                endIndex.set(startIndex);
                parseTree.AddLeaf(nextToken);
                return null;
            }
            else
            {
                String quotedArgument = isString ?
                String.format("'%s'",argument) : argument.toString();

                return new GrammarError(
                    String.format("expected %s, got %s",
                        quotedArgument,nextToken.toString()),
                    nextToken.lineNumber);
            }
        }
        else
        {
            GrammarRule grammarRule = (GrammarRule)argument;

            if(grammarRule.isSpecial)
                parseTree.BranchOut(grammarRule.name);
            else
                parseTree.BranchOut();

            GrammarError error = grammarRule.rule.Evaluate(startIndex, endIndex);
            if(error == null)
            {
                parseTree.GotoParent();
            }
            else
            {
                parseTree.DiscardBranch();
                if(grammarRule.name != null && grammarRule.isSpecial)
                    error.errorMessages.add(((GrammarRule)argument).name);
            }
            
            return error;
        }
    }

    /**
     * used for creating names for rules with brackets and comma seperators
     * @param builder append output to
     * @param listMembers array of items to make up the list
     * @param openBracket character to denote the list start
     * @param closeBracket character to denote the list end
     * @param seperator for seperating apart the members
     */
    private static void BuildBracketList(StringBuilder builder, Object[] listMembers,
        char openBracket, char closeBracket, String seperator)
    {
        builder.append(openBracket);

        boolean first = true;
        for(Object member : listMembers)
        {
            String addString = member instanceof String ?
                String.format("'%s'",member) : member.toString();
            if(first)
            {
                builder.append(addString);
                first = false;
            }
            else
            {
                builder.append(seperator);
                builder.append(addString);
            }
        }

        builder.append(closeBracket);
    }

    /**
     * the blueprint of all rules that recognise our grammar
     */
    private interface Rule
    {
        /**
         * scans through <code>allTokens</code> beginning at <code>startIndex</code>, 
         * then checks it matches a rule, and outputs to <code>endIndex</code>
         * @param startIndex where to start scanning tokens
         * @param endIndex outputs the endIndex
         * @return <code>null</code> if <code>argument</code> matches, error message otherwise
         */
        GrammarError Evaluate(int startIndex, AtomicInteger endIndex);
    }
    private Rule rule;
    private Object[] arguments;
    private String name;
    private boolean isSpecial;
    
    /**
     * sets <code>isSpecial</code> to true. Special rule's are added to the traceable errors.
     * @param name set this rule's special name
     */
    public GrammarRule(String name) {this.name = name; isSpecial = true;}
    public GrammarRule() {name = null; isSpecial = false;}

    @Override
    public String toString()
    {
        if(name != null) return name;
        else return super.toString(); 
    }

    public void SetRule(GrammarRule target)
    {
        this.rule = target.rule;
    }

    /**
     * @param setTokens tokens to check rule against
     * @param outParseTree appends the result of the parsing action to
     * @param sourceFile used to produce compilation errors
     * @throws CompilerException if there is an grammatical error
     */
    public void Evaluate(Token[] setTokens, ParseTree outParseTree, File sourceFile) throws CompilerException
    {
        allTokens = setTokens;
        parseTree = outParseTree;

        AtomicInteger endIndex = new AtomicInteger(0);
        GrammarError error = EvaluateArgument(this, 0, endIndex);

        if(error != null)
        {
            StringBuilder builder = new StringBuilder("grammar error");
            int i = ERROR_DEPTH < error.errorMessages.size() ?
                ERROR_DEPTH : error.errorMessages.size() - 1;
            
            for(; 0 <= i; i--)
                builder.append(" - "+error.errorMessages.get(i));
            
            throw new CompilerException(builder.toString(), sourceFile, error.lineNumber);
        }

        boolean isEndCorrect = endIndex.get() == setTokens.length - 1;

        if(isEndCorrect == false)
        {
            throw new CompilerException("grammar error - statements outside of class body", 
                GetToken(endIndex.get()+1));
        }
    }

    /**
     * an implementation of the <code>Rule</code> interface.
     * @param startIndex where to start scanning tokens
     * @param endIndex outputs the endIndex
     * @return <code>null</code> if <code>argument</code> matches, error message otherwise
     */
    private GrammarError ParseEmpty(int startIndex, AtomicInteger endIndex)
    {
        //don't consume any tokens
        endIndex.set(startIndex - 1);
        return null;
    }

    /**
     * an implementation of the <code>Rule</code> interface.
     * @param startIndex where to start scanning tokens
     * @param endIndex outputs the endIndex
     * @return <code>null</code> if <code>argument</code> matches, error message otherwise
     */
    private GrammarError ParseSequence(int startIndex, AtomicInteger endIndex)
    {
        AtomicInteger workingEndIndex = new AtomicInteger(endIndex.get());

        for(Object argument : arguments)
        {
            GrammarError error = EvaluateArgument(argument, startIndex, workingEndIndex);
            if(error != null)
                return error;
            startIndex = workingEndIndex.get()+1;
        }

        endIndex.set(workingEndIndex.get());
        return null;
    }

    /**
     * an implementation of the <code>Rule</code> interface.
     * @param startIndex where to start scanning tokens
     * @param endIndex outputs the endIndex
     * @return <code>null</code> if <code>argument</code> matches, error message otherwise
     */
    private GrammarError ParseUniqueSequence(int startIndex, AtomicInteger endIndex) 
    {
        AtomicInteger workingEndIndex = new AtomicInteger(endIndex.get());
        boolean isCritical = false;

        for(Object argument : arguments)
        {
            GrammarError error = EvaluateArgument(argument, startIndex, workingEndIndex);
            if(error != null)
            {
                if(isCritical)
                    error.isCritical = true;
                return error;
            }
            startIndex = workingEndIndex.get()+1;
            isCritical = true;
        }

        endIndex.set(workingEndIndex.get());
        return null;
    }

    /**
     * an implementation of the <code>Rule</code> interface.
     * @param startIndex where to start scanning tokens
     * @param endIndex outputs the endIndex
     * @return <code>null</code> if <code>argument</code> matches, error message otherwise
     */
    private GrammarError ParseChoice(int startIndex, AtomicInteger endIndex)
    {
        for(Object argument : arguments)
        {
            GrammarError error = EvaluateArgument(argument, startIndex, endIndex);
            if(error == null)
                return null;
            else if(error.isCritical)
                return error;
        }

        StringBuilder builder = new StringBuilder("expected ");
        builder.append(toString());

        if(GetToken(startIndex) != null)
            builder.append(", got "+GetToken(startIndex).toString());
        
        return new GrammarError(builder.toString(), GetLine(startIndex));
    }

    /**
     * an implementation of the <code>Rule</code> interface.
     * @param startIndex where to start scanning tokens
     * @param endIndex outputs the endIndex
     * @return <code>null</code> if <code>argument</code> matches, error message otherwise
     */
    private GrammarError ParseZeroOrOne(int startIndex, AtomicInteger endIndex)
    {
        GrammarError error = EvaluateArgument(arguments[0], startIndex, endIndex);
        if(error != null)
        {
            if(error.isCritical)
                return error;
            else
                //don't consume any tokens
                endIndex.set(startIndex - 1);
        }
        
        return null;
    }

    /**
     * an implementation of the <code>Rule</code> interface.
     * @param startIndex where to start scanning tokens
     * @param endIndex outputs the endIndex
     * @return <code>null</code> if <code>argument</code> matches, error message otherwise
     */
    private GrammarError ParseOneOrMore(int startIndex, AtomicInteger endIndex)
    {
        boolean trueOnce = false;
        GrammarError error;
        while((error = EvaluateArgument(arguments[0], startIndex, endIndex)) == null)
        {
            trueOnce = true;
            startIndex = endIndex.get() + 1;
        }

        if(error.isCritical)
            return error;

        if(trueOnce)
            return null;
        else
            return error;
    }

    /**
     * an implementation of the <code>Rule</code> interface.
     * @param startIndex where to start scanning tokens
     * @param endIndex outputs the endIndex
     * @return <code>null</code> if <code>argument</code> matches, error message otherwise
     */
    private GrammarError ParseZeroOrMore(int startIndex, AtomicInteger endIndex)
    {
        GrammarError error;
        while((error = EvaluateArgument(arguments[0], startIndex, endIndex)) == null)
            startIndex = endIndex.get() + 1;

        if(error.isCritical)
            return error;
        //don't consume a token, this operation is only needed when its zero matches
        endIndex.set(startIndex-1);
        return null;
    }
}