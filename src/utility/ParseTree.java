package utility;

import java.util.ArrayList;

public class ParseTree
{
    public class Node //not leaves
    {
        public Node parent;
        public ArrayList<Object> children;
        /** contains the rule's special name (for debugging) */
        public String name;

        public Node(Node parent)
        {
            this.parent = parent; this.children = new ArrayList<Object>();
        }

        @Override
        public String toString() //useful for debugging
        {
            StringBuilder builder = new StringBuilder();
            builder.append(name+" : ");
            builder.append("{ ");            
            boolean first = true;
            for (Object object : children)
            {
                String objectName;
                if(object instanceof Token)
                    objectName = String.format("'%s'",((Token)object).lexeme);
                else
                    objectName = object.toString();
                
                if(first)
                {
                    builder.append(objectName);
                    first = false;
                }
                else
                    builder.append(", "+objectName);
            }
            builder.append(" }");
            return builder.toString();
        }
    }

    public Node root;
    private Node currentSubtree;

    public ParseTree()
    {
        currentSubtree = root = new Node(null);
    }

    /**
     * add a new leaf on the current branch
     * @param leaf a terminal
     */
    public void AddLeaf(Token leaf)
    {
        currentSubtree.children.add(leaf);
    }
    
    /**
     * destroy the current branch and goes to parent
     */
    public void DiscardBranch()
    {
        currentSubtree.children.clear();
        GotoParent();
    }

    /**
     * moves into the current branch's parent and trims the tree
     */
    public void GotoParent()
    {
        if(currentSubtree.parent == null)
            throw new RuntimeException("root node has no parent to goto");
        else
        {
            //trim your brush
            if(currentSubtree.children.size() == 0)
            {
                currentSubtree.parent.children.remove(currentSubtree);
            }
            else if(currentSubtree.children.size() == 1)
            {
                currentSubtree.parent.children.remove(currentSubtree);
                currentSubtree.parent.children.add(currentSubtree.children.get(0));
            }
            else if(currentSubtree.name == null)
            {
                currentSubtree.parent.children.remove(currentSubtree);
                currentSubtree.parent.children.addAll(currentSubtree.children);
            }

            currentSubtree = currentSubtree.parent;
        }
    }

    /**
     * create a new branch with a special rule
     * @param childName name of the special rule
     */
    public void BranchOut(String childName)
    {
        Node child = new Node(currentSubtree);
        child.name = childName;
        currentSubtree.children.add(child);
        currentSubtree = child;
    }

    /**
     * create a new branch with a non-special rule
     */
    public void BranchOut()
    {
        Node child = new Node(currentSubtree);
        currentSubtree.children.add(child);
        currentSubtree = child;
    }

    @Override
    public String toString()
    {
        return root.toString();
    }

    /** clear the whole structure */
    public void Clear()
    {
        currentSubtree = root = new Node(null);
    }
}