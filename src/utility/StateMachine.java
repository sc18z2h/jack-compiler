package utility;

import java.util.HashSet;

/** represents a finite state machine */
public class StateMachine
{
    public interface TransitionFunction
    {
        /**
         * computes the next state given a current state and a input
         * @param currentState the current state before the transition
         * @param input the next input
         * @return the new state
         */
        int Delta(int currentState, char input);
    }

    private int state;
    private TransitionFunction transition;
    /** a machine is 'finished' if its state is in this set */
    private HashSet<Integer> finalStateSet;
    
    /**
     * create a new machine. with a entry state of 0.
     * @param transitionFunction a single function recognising all transitions
     * @param finalStates a list of final state numbers
     */
    public StateMachine(TransitionFunction transitionFunction, int... finalStates)
    {        
        transition = transitionFunction;        
        Reset();

        if(finalStates != null)
        {
            finalStateSet = new HashSet<Integer>(finalStates.length);        
                for(int i : finalStates)
                    finalStateSet.add(i);
        }
        else
            finalStateSet = new HashSet<Integer>(0);
    }

    /** reset the machine to the entry state */
    public void Reset()
    {
        state = 0;
    }

    /**
     * process the next input
     * @param input character
     */
    public void Step(char input)
    {
        state = transition.Delta(state, input);
    }

    /**
     * @return true if its state is in a final state, false otherwise
     */
    public boolean InFinalState()
    {
        return finalStateSet.contains(state);
    }

    /**
     * @return the current state
     */
    public int GetState()
    {
        return state;
    }
}