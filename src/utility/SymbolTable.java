package utility;

import java.util.*;

public class SymbolTable
{
    /** represents all of user-defined classes, built-in classes and primitive types. */
    public class TypeSymbol
    {
        /** the name token of the class declaration. null represents builtin types */
        public Token source;

        public String name;

        /**
         * create a builtin type with no source
         * @param builtinName
         */
        public TypeSymbol(String builtinName) {name = builtinName;}
        public TypeSymbol(Token source, String name)
        {
            this.source = source;
            this.name = name;
        }
    }

    /** represents user-defined classes and built-in classes */
    public class ClassSymbol extends TypeSymbol
    {
        /** a counter for the current number of static fields */
        public int staticVariables;
        /** a counter for the current number of non-static fields */
        public int nonStaticFields;
        public ArrayList<FieldSymbol> fields;
        public ArrayList<MethodSymbol> methods;
        public ClassSymbol(Token source, String name)
        {            
            super(source, name);
            staticVariables = nonStaticFields = 0;
            fields = new ArrayList<>();
            methods = new ArrayList<>();
        }
    }

    /** represents a variable, can be a parameter (with isParameter = true) or a scoped variable or a field */
    public class VariableSymbol
    {
        public int address;
        public boolean isParameter;
        public Token source;

        public TypeSymbol type;
        public String name;
        public VariableSymbol(boolean isParameter, Token source, TypeSymbol type, String name)
        {
            this.isParameter = isParameter;
            this.source = source;
            this.type = type;
            this.name = name;
        }
    }

    /** represents a class field, has the isStatic property */
    public class FieldSymbol extends VariableSymbol
    {
        public boolean isStatic;
        public FieldSymbol(Token source, boolean isStatic, TypeSymbol type, String name)
        {
            super(false, source, type, name);
            this.isStatic = isStatic;
        }
    }

    /** represents class methods */
    public class MethodSymbol
    {
        public Token source;

        public boolean isStatic;
        public boolean isConstructor;
        /** null would represent void */
        public TypeSymbol returnType;
        public String name;
        public List<VariableSymbol> parameters;

        public MethodSymbol(
            Token source,
            boolean isStatic,
            boolean isConstructor,
            TypeSymbol returnType,
            String name,
            List<VariableSymbol> parameters)
        {
            this.source = source;
            this.isStatic = isStatic;
            this.isConstructor = isConstructor;
            this.returnType = returnType;
            this.name = name;
            this.parameters = parameters;
        }
    }

    /** constant table - used for reseting the table */
    public List<TypeSymbol> builtinTypes = new ArrayList<>();
    /** the main table - contains all builtin primitives, builtin classes and user classes */
    public HashMap<String,TypeSymbol> allTypes = new HashMap<>();

    //these changed between different files
    /** contains all the variables in the current class */
    public HashMap<String,FieldSymbol> classVariables = new HashMap<>();
    /** contains all the methods in the current class */
    public HashMap<String,MethodSymbol> classMethods = new HashMap<>();

    /** contains all the parameters and scoped variables in the current method 
     * changes for every class method
    */
    public HashMap<String,VariableSymbol> methodVariables = new HashMap<>();

    /** variables in this structure are used to remove from the methodVariables map */
    private Stack<List<String>> scopedVariables = new Stack<>();

    /** the current local segment address
     * can increase when declaring local variables
     * or decrease when exiting a scope - since a scope's variables are free to use outside of the scope
     */
    private int localAddress;
    /** the maximum number achieved by local address, used for declaring the VM function */
    private int maxLocalAddress;

    /**
     * adds some types to the table
     * @param types a list of type symbols
     */
    public void AddTypes(TypeSymbol ... types)
    {
        for(TypeSymbol builtinType : types)
            allTypes.put(builtinType.name, builtinType);
    }

    /**
     * sets all the current types declared as builtin classes
     */
    public void SetBuiltinClasses()
    {
        for(Map.Entry<String,TypeSymbol> entry : allTypes.entrySet())
            builtinTypes.add(entry.getValue());
    }

    /**
     * clear all types in the table, and add the builtin types back in
     */
    public void ResetTables()
    {
        allTypes.clear();
        classVariables.clear();
        classMethods.clear();
        methodVariables.clear();

        for(TypeSymbol builtinType : builtinTypes)
            allTypes.put(builtinType.name, builtinType);
    }

    /**
     * change the table to reflect on a new method context
     * @param originClass the class of the method
     * @param method the method in question
     */
    public void SwitchMethod(ClassSymbol originClass, MethodSymbol method)
    {
        classVariables.clear();
        classMethods.clear();
        methodVariables.clear();
        scopedVariables.clear();
        localAddress = maxLocalAddress = 0;

        for(FieldSymbol field : originClass.fields)
                classVariables.put(field.name, field);
        for(MethodSymbol classMethod : originClass.methods)
                classMethods.put(classMethod.name, classMethod);

        //only non-static methods and constructors have access to "this"
        if(method.isStatic == false || method.isConstructor)
            methodVariables.put("this", new FieldSymbol(null, false, originClass, "this"));

        for(VariableSymbol parameter : method.parameters)
            methodVariables.put(parameter.name, parameter);
    }

    /** create a new record for a scope, so that the added scoped variables can be removed later */
    public void NewScope()
    {
        scopedVariables.push(new ArrayList<String>());
    }
    
    /** remove the current scope's variables */
    public void ExitScope()
    {
        List<String> variablesToRemove = scopedVariables.pop();
        for(String variable : variablesToRemove)
        {
            methodVariables.remove(variable);
            localAddress--;
        }
    }

    /**
     * add to the current scope's variable list
     * @param variable the variable to add
     */
    public void AddScopedVariable(VariableSymbol variable)
    {
        methodVariables.put(variable.name, variable);
        if(variable.isParameter == false)
        {
            variable.address = localAddress;
            localAddress++;
            if(maxLocalAddress < localAddress)
                maxLocalAddress = localAddress;
        }

        if(0 < scopedVariables.size())
            scopedVariables.peek().add(variable.name);
    }

    public int GetMaxLocalVariables()
    {
        return maxLocalAddress;
    }
}