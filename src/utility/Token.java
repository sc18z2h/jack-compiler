package utility;

import java.io.File;

/**
 * holds data about individual elements extracted from a jack source file.
 * also contains its origin so it can be traced back.
 */
public class Token
{
    public enum TokenTypes
    {
        invalid,
        symbol,
        keyword,
        constant,
        string,
        identifier
    };
    
    public TokenTypes type;
    public String lexeme;
    public int lineNumber;
    public File sourceFile;

    public Token(TokenTypes type, String lexeme, int lineNumber, File sourceFile)
    {        
        this.type = type;
        this.lexeme = lexeme;
        this.lineNumber = lineNumber;
        this.sourceFile = sourceFile;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null ||
            !Token.class.isAssignableFrom(obj.getClass()))
            return false;
        
        Token other = (Token) obj;
        return type.equals(other.type) && lexeme.equals(other.lexeme);
    }

    @Override
    public String toString()
    {
        String typeName = type == null ? "null" : type.toString();
        return String.format("(%s, '%s')",typeName,lexeme);
    }
}